const url = 'https://staging.gm.anujsulakhiya.codes';
const apiUrl =  url + "/api";

export const environment = {
  production: true,
  appUrl: url,
  apiUrl
};
