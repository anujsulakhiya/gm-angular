const url =  'http://api.shantivatika.com';
const apiUrl =  url + "/api";

export const environment = {
  production: true,
  appUrl: url,
  apiUrl
};
