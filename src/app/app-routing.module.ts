import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {Error404Component} from './components/error404/error404.component';
import {PublicGuard} from "./guards/public.guard";
import {DashboardGuard} from "./guards/dashboard.guard";
import { InvoiceFormateComponent } from './Dashboard/Modules/customer-ledger/invoice-formate/invoice-formate.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'Public',
    pathMatch: 'full',
  },
  {
    path: 'PrintInvoice/:invoiceId', canLoad: [DashboardGuard], component: InvoiceFormateComponent
  },
  {
    path: 'Public',
    canLoad: [PublicGuard],
    loadChildren: () => import('./Public/public.module').then( m => m.PublicModule ),
  },
  {
    path: 'Dashboard',
    canLoad: [DashboardGuard],
    loadChildren: () => import('./Dashboard/dashboard.module').then(m => m.DashboardModule)
  },
  {
    path: '**',
    component: Error404Component
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
