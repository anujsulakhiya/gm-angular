import { Component } from '@angular/core';
import { GlobalService } from '../../services/global.service';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.css'],
})
export class LoaderComponent {
  isLoading: any;

  constructor(private loaderService: GlobalService) {
    this.loaderService.isLoading.subscribe((v) => {
      this.isLoading = v;
    });
  }
}
