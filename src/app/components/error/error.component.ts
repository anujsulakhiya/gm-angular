import { Component, OnDestroy, OnInit } from '@angular/core';
import { GlobalService } from '../../services/global.service';
import { interval, Subscription } from 'rxjs';

@Component({
  selector: 'app-no-internet',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.css'],
})
export class ErrorComponent implements OnInit, OnDestroy {
  isConnected: any;
  noInternetConnection = false;

  subscription: Subscription | any;
  intervalId: any;

  constructor(private globalService: GlobalService) {
    this.checkError();
  }

  checkError() {
    this.globalService.httpError.subscribe((v) => {
      this.noInternetConnection = v;
    });
  }

  ngOnInit() {
    this.setInterval(50000);
  }

  setInterval(time: number) {
    const source = interval(time);
    this.subscription = source.subscribe((val) => this.checkConnection());
  }

  checkConnection() {
    console.log('cheking Connection');
    this.globalService.checkConnection().subscribe(
      () => {
        this.noInternetConnection = false;
        this.subscription && this.subscription.unsubscribe();
      },
      () => {
        this.noInternetConnection = true;
      }
    );
  }

  ngOnDestroy() {
    this.subscription && this.subscription.unsubscribe();
  }
}
