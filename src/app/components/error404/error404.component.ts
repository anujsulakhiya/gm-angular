import {Component, OnInit} from '@angular/core';
import {SessionService} from "../../services/session.service";

@Component({
  selector: 'app-error404',
  templateUrl: './error404.component.html',
  styleUrls: ['./error404.component.css']
})
export class Error404Component {

  constructor(private SessionService:SessionService) {
    if(!this.SessionService.getItem('access')){
      this.SessionService.destroySession();
    }
  }

  onLogout() {
    this.SessionService.destroySession();
  }
}
