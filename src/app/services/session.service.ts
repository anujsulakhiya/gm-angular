import {Injectable} from '@angular/core';
import {LoginSuccessInterface, UserInterface} from '../Public/public.interfaces';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  accessTokenKeyName = 'access';
  userKeyName = 'user';
  storage = localStorage;

  constructor(
    private router: Router
  ) { }

  setSession(authResponse: LoginSuccessInterface) {
    this.setItem(this.accessTokenKeyName, authResponse.access_token);
    // this.setItem(this.refreshTokenKeyName, authResponse.refresh);
    this.setItem(this.userKeyName, JSON.stringify(authResponse.user));
    this.navigate('/Dashboard/');
  }


  // reNavigate(route: string) {
  //   this.setCurrentPage(route);
  //   window.location.replace(route);
  // }

  setCurrentPage(url: string){
    this.setItem('currentPage', url);
  }

  setItem(key: string, value: string) {
    this.storage.setItem(key, value);
  }

  getItem(key: string): string | null{
    return this.storage.getItem(key);
  }

  destroySession() {
    this.deleteAllItem();
    this.navigate('/Public/login');
  }

  deleteAllItem() {
    this.storage.clear();
  }


  getUser(): UserInterface {
    return JSON.parse(<string>this.getItem(this.userKeyName)) as UserInterface;
  }

  getAccessToken(): string|null {
    return this.getItem('access');
  }
  //
  // getRefreshToken(): string {
  //   return this.getItem(this.refreshTokenKeyName);
  // }

  isAuthenticated() {
    return !!localStorage.getItem('access');
  }

  navigate(route: string) {
    this.router.navigate([route]).then( () => {
      this.setCurrentPage(route);
    });
  }

}
