import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  apiUrl = environment.apiUrl

  constructor(private _http: HttpClient) { }

  get(url: string): any{
    return this._http.get(this.apiUrl + url);
  }

  getWithParams(url: string, params: any): any{
    // HttpParams
    return this._http.get(this.apiUrl + url, {params: params});
  }

  post(url: string, data: any): any{
    return this._http.post(this.apiUrl + url, data);
  }

  put(url: string, data: any): any{
    return this._http.put(this.apiUrl + url, data);
  }

  patch(url: string, data: any): any{
    return this._http.patch(this.apiUrl + url, data);
  }

  delete(url: string){
    return this._http.delete(this.apiUrl + url);
  }

}
