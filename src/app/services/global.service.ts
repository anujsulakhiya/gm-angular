import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpService } from './http.service';
import {DatePipe} from "@angular/common";

@Injectable({
  providedIn: 'root',
})
export class GlobalService {



  public isLoading = new BehaviorSubject(false);
  public error = new BehaviorSubject(false);
  public httpError = new BehaviorSubject(false);
  public badRequest_400 = new BehaviorSubject(false);
  public unauthorized_401 = new BehaviorSubject(false);
  public forbidden_403 = new BehaviorSubject(false);
  public notFound_404 = new BehaviorSubject(false);
  public timeOut_408 = new BehaviorSubject(false);
  public conflict_409 = new BehaviorSubject(false);
  public internalServerError_500 = new BehaviorSubject(false);

  static badRequest = 400;
  static unauthorized = 401;
  static forbidden = 403;
  static notFound = 404;
  static timeOut = 408;
  static conflict = 409;
  static internalServerError = 500;

  constructor(private httpService: HttpService) {}

  checkConnection(): Observable<any> {
    return this.httpService.get('/auth/check_connection');
  }

  // transformDate(date: any){
  //   return this.datePipe.transform(date, 'yyyy-MM-dd')
  // }
}
