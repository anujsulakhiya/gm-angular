import {Injectable, TemplateRef} from '@angular/core';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class AlertService {


  toasts: any[] = [];

  constructor() { }


  show(textOrTpl: string | TemplateRef<any>, options: any = {}) {
    this.toasts.push({ textOrTpl, ...options });
  }

  remove(toast:any) {
    this.toasts = this.toasts.filter(t => t !== toast);
  }

  showSuccess() {
    this.show('I am a success toast', { classname: 'bg-success text-light', delay: 10000 });
  }

  showDanger(dangerTpl:any) {
    this.show(dangerTpl, { classname: 'bg-danger text-light', delay: 15000 });
  }

  alert(title:string,htmlMsg:string,icon:any){
    Swal.fire(title, htmlMsg, icon)
  }
}
