import {Injectable, TemplateRef} from '@angular/core';
import {ToastrService} from "ngx-toastr";

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  toasts: any[] = [];

  constructor(private toastr: ToastrService) { }




  show(textOrTpl: string | TemplateRef<any>, options: any = {}) {
    this.toasts.push({ textOrTpl, ...options });
  }

  remove(toast:any) {
    this.toasts = this.toasts.filter(t => t !== toast);
  }

  showSuccess(message:any){
    this.show(message, { classname: 'bg-success text-light', delay: 15000 });
  }

  showError(message:any, title: any){

    this.toastr.error(message, title)
  }

  showInfo(message: string, title: string){

    this.toastr.info(message, title)
  }

  showWarning(message: string, title: string){

    this.toastr.warning(message, title)
  }
}
