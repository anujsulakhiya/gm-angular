import { Injectable } from '@angular/core';
import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { GlobalService } from '../services/global.service';

export class HttpError {
  static badRequest = 400;
  static unauthorized = 401;
  static forbidden = 403;
  static notFound = 404;
  static timeOut = 408;
  static conflict = 409;
  static internalServerError = 500;
}

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {
  constructor(public globalService: GlobalService) {}

  resetApp() {
    console.log('reset');
  }
  updateStatus(status: boolean) {
    this.globalService.httpError.next(status);
  }

  intercept(
    request: HttpRequest<unknown>,
    next: HttpHandler
  ): Observable<HttpEvent<unknown>> {
    const logFormat = 'background: maroon; color: white';

    return next.handle(request).pipe(
      tap(
        () => {
          this.updateStatus(false);
        },
        (exception) => {
          if (exception instanceof HttpErrorResponse) {
            switch (exception.status) {
              case HttpError.badRequest:
                this.updateStatus(true);
                console.log('%c Bad Request 400', logFormat);
                break;

              case HttpError.unauthorized:
                this.updateStatus(true);
                console.log('%c Unauthorized 401', logFormat);
                // window.location.href = '/login' + window.location.hash;
                this.resetApp();
                break;

              case HttpError.notFound:
                this.updateStatus(true);
                //show error toast message
                console.log('%c Not Found 404', logFormat);
                this.resetApp();
                break;

              case HttpError.timeOut:
                this.updateStatus(true);
                // Handled in AnalyticsExceptionHandler
                console.log('%c TimeOut 408', logFormat);
                this.resetApp();
                break;

              case HttpError.forbidden:
                this.updateStatus(true);
                console.log('%c Forbidden 403', logFormat);
                this.resetApp();
                break;

              case HttpError.internalServerError:
                this.updateStatus(true);
                console.log('%c big bad 500', logFormat);
                this.resetApp();
                break;
            }
          }
        }
      )
    );
  }
}
