export interface LoginInterface {
  email: string,
  password: string
}

export interface RegisterInterface {
  name: string,
  email: string,
  password: string
}

export interface UserInterface {
  id: number
  name: string
  email: string
  created_at?:string
}


export interface LoginSuccessInterface {
  access_token: string
  user: UserInterface
}

export interface ChangePasswordInterface {
  password: string
  newPassword: string
  confirmNewPassword: string
}
