import {Injectable} from '@angular/core';
import {HttpService} from '../services/http.service';
import {LoginInterface, LoginSuccessInterface, RegisterInterface} from './public.interfaces';
import {Observable} from 'rxjs';


@Injectable({
  providedIn: 'any'
})
export class PublicService {

  constructor(private httpService: HttpService) { }

  login(data: LoginInterface): Observable<LoginSuccessInterface>{
    return this.httpService.post('/auth/login', data);
  }

  register(data: RegisterInterface): Observable<LoginSuccessInterface>{
    return this.httpService.post('/auth/register', data);
  }

}
