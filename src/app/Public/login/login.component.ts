import {Component} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {PublicService} from '../public.service';
import {SessionService} from '../../services/session.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css' , '../public.component.css']
})
export class LoginComponent {

  loginForm : FormGroup|any ;
  errors = [];
  showError = false;

  constructor(private publicService: PublicService,
              private formBuilder: FormBuilder,
              private SessionService: SessionService) {
    this.initializeForm();
  }

  initializeForm(){
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required]],
      password: ['', [Validators.required, Validators.minLength(6)]]
    })
  }
  onLogin(){
    this.showError = false;
    if ( this.loginForm.valid ){
      this.publicService.login(this.loginForm.value).subscribe( res => {
        this.SessionService.setSession(res);
      }, (err) => {
        this.showError = true;
      });
    }
  }

}
