import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {PublicRoutingModule} from './public-routing.module';
import {LoginComponent} from './login/login.component';
import {PublicComponent} from './public.component';

import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {PublicService} from './public.service';


@NgModule({
  declarations: [
    LoginComponent,
    PublicComponent
  ],
  providers: [
    PublicService
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    PublicRoutingModule
  ]
})
export class PublicModule { }
