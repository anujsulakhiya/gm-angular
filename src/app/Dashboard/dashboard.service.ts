import { Injectable } from '@angular/core';
import { HttpService } from '../services/http.service';
import { ChangePasswordInterface } from '../Public/public.interfaces';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class DashboardService {
  constructor(private httpService: HttpService) {}

  changePassword(data: ChangePasswordInterface): Observable<any> {
    return this.httpService.patch('/auth/changePassword', data);
  }

  getHomeDetails():Observable<any>{
    return this.httpService.get('/getHomeDetails');
  }



}
