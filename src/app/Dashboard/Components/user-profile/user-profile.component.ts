import {Component} from '@angular/core';
import {SessionService} from '../../../services/session.service';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent {

  constructor( private sessionService: SessionService) { }

  user = this.sessionService.getUser();


}
