import {Component} from '@angular/core';
import {SessionService} from '../../../services/session.service';
import { DashboardService } from '../../dashboard.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {


  home:any;
  constructor(private sessionService: SessionService,
              private dashboardService: DashboardService) {

                this.getHomeDetails();
  }

  getHomeDetails(){
    this.dashboardService.getHomeDetails().subscribe( res => {
      this.home =res;
    });
  }



}
