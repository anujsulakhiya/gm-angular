import {Component} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DashboardService} from '../../dashboard.service';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent {

  changePassForm: FormGroup | any;
  sameOldPassword = false;
  oldPasswordMismatch = false;
  sameNewPassword = false;
  changeSuccess = false;

  constructor( private dashboardServices: DashboardService, private formBuilder: FormBuilder ) {
    this.initializeForm();
  }

  initializeForm() {
    this.changePassForm = this.formBuilder.group({
      password: ['', [Validators.required, Validators.minLength(6)]],
      newPassword: ['', [Validators.required, Validators.minLength(6)]],
      confirmNewPassword: ['', [Validators.required, Validators.minLength(6)]],
    })
  }

  doChangePassword() {
    this.oldPasswordMismatch = false;
    this.sameOldPassword = false;
    this.sameNewPassword = false;
    this.changeSuccess = false;
    if (this.changePassForm.valid) {
      this.dashboardServices.changePassword(this.changePassForm.value).subscribe( () => {
        this.changeSuccess = true;
      }, (err) => {
        if (err instanceof HttpErrorResponse) {

          if(err.error.errors.password){
            this.oldPasswordMismatch = true;
          }

          if(err.error.errors.newPassword && !err.error.errors.password){
            this.sameOldPassword = true;
          }

          if(err.error.errors.confirmNewPassword && !err.error.errors.password){
            this.sameNewPassword = true;
          }
        }
      });
    }
  }

}
