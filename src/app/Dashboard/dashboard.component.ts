import {Component} from '@angular/core';
import {SessionService} from '../services/session.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent  {

  Title = 'CampusMate';

  constructor(private sessionService:SessionService) {}


}
