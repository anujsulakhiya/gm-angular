import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MasterRoutingModule } from './master-routing.module';
import { MasterComponent } from './master.component';
import { MasterHomeComponent } from './master-home/master-home.component';
import {ReactiveFormsModule} from "@angular/forms";
import { StockMasterComponent } from './stock-master/stock-master.component';


@NgModule({
  declarations: [
    MasterComponent,
    MasterHomeComponent,
    StockMasterComponent
  ],
  imports: [
    CommonModule,
    MasterRoutingModule,
    ReactiveFormsModule
  ]
})
export class MasterModule { }
