import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MasterComponent } from './master.component';
import { MasterHomeComponent } from './master-home/master-home.component';

const routes: Routes = [
  {
    path: '',
    component: MasterComponent,
    children: [
      {
        path: '',
        redirectTo: 'MastersHome',
        pathMatch: 'full',
      },
      {
        path: 'MastersHome',
        component: MasterHomeComponent,
      },
      {
        path: 'FirmMaster',
        loadChildren: () =>
          import('./firm-master/firm-master.module').then(
            (m) => m.FirmMasterModule
          ),
      },
      {
        path: 'StockMaster',
        loadChildren: () =>
          import('./stock-master/stock-master.module').then(
            (m) => m.StockMasterModule
          ),
      },
      {
        path: 'CustomerMaster',
        loadChildren: () =>
          import('./customer-master/customer-master.module').then(
            (m) => m.CustomerMasterModule
          ),
      },
      {
        path: 'BankMaster',
        loadChildren: () =>
          import('./bank-master/bank-master.module').then(
            (m) => m.BankMasterModule
          ),
      },
      
      {
        path: 'MenuMaster',
        loadChildren: () =>
          import('./menu-master/menu-master.module').then(
            (m) => m.MenuMasterModule
          ),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MasterRoutingModule {}
