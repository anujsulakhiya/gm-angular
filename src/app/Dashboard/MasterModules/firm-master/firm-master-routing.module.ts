import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {FirmMasterComponent} from './firm-master.component';
import {CreateFirmComponent} from './create-firm/create-firm.component';
import {EditFirmComponent} from './edit-firm/edit-firm.component';
import {FirmListComponent} from './firm-list/firm-list.component';

const routes: Routes = [
  {
    path: '', component: FirmMasterComponent, children: [
      {
        path: '', redirectTo: 'FirmList', pathMatch: 'full'
      },
      {
        path: 'CreateFirm', component: CreateFirmComponent
      },
      {
        path: 'EditFirm/:id', component: EditFirmComponent
      },
      {
        path: 'FirmList', component: FirmListComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FirmMasterRoutingModule { }
