import {Injectable} from '@angular/core';
import {HttpService} from '../../../services/http.service';
import {CreateFirmInterface, FirmInterface} from '../../dashboard.interfaces';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FirmMasterService {

  constructor(private httpService: HttpService) { }

  createFirm(data: CreateFirmInterface): Observable<FirmInterface>{
    return this.httpService.post('/master/firm',data);
  }

  editFirm(data: CreateFirmInterface,id:number): Observable<FirmInterface>{
    return this.httpService.put('/master/firm/'+id,data);
  }

  getFirm(id:number): Observable<FirmInterface[]>{
    return this.httpService.get('/master/firm/'+id);
  }

  getAllFirm(): Observable<FirmInterface[]>{
    return this.httpService.get('/master/firm');
  }

  deleteFirm(id:number): Observable<any>{
    return this.httpService.delete('/master/firm/'+id);
  }


}
