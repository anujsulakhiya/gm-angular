import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FirmMasterService } from '../firm-master.service';
import { HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { FirmInterface } from '../../../dashboard.interfaces';

@Component({
  selector: 'app-edit-firm',
  templateUrl: './edit-firm.component.html',
  styleUrls: ['./edit-firm.component.css'],
})
export class EditFirmComponent {
  editFirm: FormGroup | any;
  firmDetails: FirmInterface | any = [];
  firmUpdateSuccess = false;
  existingFirmName = false;
  firmId = 0;

  constructor(
    private firmService: FirmMasterService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute
  ) {
    this.initializeForm(this.firmDetails);
    this.getRouteParam();
  }

  getRouteParam() {
    this.route.params.subscribe((params) => {
      this.firmId = params.id;
      this.getFirmDetails(params.id);
    });
  }

  initializeForm(firmDetails: FirmInterface) {
    this.editFirm = this.formBuilder.group({
      firm_name: [firmDetails.firm_name, [Validators.required]],
      firm_contact_no: [firmDetails.firm_contact_no, [Validators.required,Validators.pattern('^((\\+91-?)|0)?[0-9]{10}$')]],
      firm_address: [firmDetails.firm_address, [Validators.required]],
      firm_gstin: [firmDetails.firm_gstin, [Validators.nullValidator]],
    });
  }

  getFirmDetails(id: number) {
    this.firmService.getFirm(id).subscribe((res) => {
      this.firmDetails = res;
      this.initializeForm(this.firmDetails);
    });
  }

  editFirmMaster() {
    if (this.editFirm.valid) {
      this.firmService.editFirm(this.editFirm.value, this.firmId).subscribe(
        (res) => {
          this.firmUpdateSuccess = true;
        },
        (err) => {
          if (err instanceof HttpErrorResponse) {
            if (err.error.errors.firm_name) {
              this.existingFirmName = true;
            }
          }
        }
      );
    }
  }
}
