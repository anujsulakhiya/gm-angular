import { TestBed } from '@angular/core/testing';

import { FirmMasterService } from './firm-master.service';

describe('FirmMasterService', () => {
  let service: FirmMasterService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FirmMasterService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
