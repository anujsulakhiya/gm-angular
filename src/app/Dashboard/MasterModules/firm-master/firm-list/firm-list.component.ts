import {Component} from '@angular/core';
import {FirmMasterService} from '../firm-master.service';
import {FirmInterface} from '../../../dashboard.interfaces';
import {AlertService} from "../../../../services/alert.service";

@Component({
  selector: 'app-firm-list',
  templateUrl: './firm-list.component.html',
  styleUrls: ['./firm-list.component.css']
})
export class FirmListComponent {

  firmData: FirmInterface|any = [];
  noFirmFound = false;
  deleteFirmSuccess = false;

  constructor(private firmService: FirmMasterService,
              private sweetAlert: AlertService) {
    this.getAllFirm();
  }

  getAllFirm(){
    this.firmService.getAllFirm().subscribe( res => {
      this.firmData = [];
      if(res.length > 0){
        this.firmData = res;
      } else  {
        this.noFirmFound = true;
        this.sweetAlert.alert('No Firm Found','Please Create Firm From Firm Master Section','warning');
      }

    })
  }

  deleteFirm(id:number){
    this.firmService.deleteFirm(id).subscribe( () => {
      this.deleteFirmSuccess = true;
      this.getAllFirm();
    })
  }

}
