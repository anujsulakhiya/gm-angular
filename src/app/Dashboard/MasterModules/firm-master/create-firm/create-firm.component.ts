import {Component, ViewChild} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FirmMasterService } from '../firm-master.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-create-firm',
  templateUrl: './create-firm.component.html',
  styleUrls: ['./create-firm.component.css'],
})
export class CreateFirmComponent {


  createFirm: FormGroup | any;
  firmCreatedSuccess = false;
  existingFirmName = false;

  constructor(
    private firmService: FirmMasterService,
    private formBuilder: FormBuilder
  ) {
    this.initializeForm();
  }

  @ViewChild('firmName') firmName:any

  initializeForm() {
    this.createFirm = this.formBuilder.group({
      firm_name: ['', [Validators.required]],
      firm_contact_no: ['', [Validators.required,Validators.pattern('^((\\+91-?)|0)?[0-9]{10}$')]],
      firm_address: ['', [Validators.required]],
      firm_gstin: ['', [Validators.nullValidator]],
    });
  }

  createFirmMaster() {
    this.firmCreatedSuccess = this.existingFirmName = false;
    if (this.createFirm.valid) {
      this.firmService.createFirm(this.createFirm.value).subscribe(
        () => {
          this.firmCreatedSuccess = true;


          this.initializeForm();
          this.firmName.focus();
        },
        (err) => {
          if (err instanceof HttpErrorResponse) {
            if (err.error.errors.firm_name) {
              this.existingFirmName = true;
            }
          }
        }
      );
    }
  }
}
