import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {FirmMasterRoutingModule} from './firm-master-routing.module';
import {CreateFirmComponent} from './create-firm/create-firm.component';
import {EditFirmComponent} from './edit-firm/edit-firm.component';
import {FirmListComponent} from './firm-list/firm-list.component';
import {FirmMasterComponent} from './firm-master.component';
import {ReactiveFormsModule} from '@angular/forms';


@NgModule({
  declarations: [
    CreateFirmComponent,
    EditFirmComponent,
    FirmListComponent,
    FirmMasterComponent
  ],
  imports: [
    CommonModule,
    FirmMasterRoutingModule,
    ReactiveFormsModule
  ]
})
export class FirmMasterModule { }
