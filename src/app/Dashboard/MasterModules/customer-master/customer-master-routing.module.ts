import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CustomerMasterComponent } from './customer-master.component';
import { CreateCustomerComponent } from './create-customer/create-customer.component';
import { EditCustomerComponent } from './edit-customer/edit-customer.component';
import { CustomerListComponent } from './customer-list/customer-list.component';

const routes: Routes = [
  {
    path: '',
    component: CustomerMasterComponent,
    children: [
      {
        path: '',
        redirectTo: 'CreateCustomer',
        pathMatch: 'full',
      },
      {
        path: 'CreateCustomer',
        component: CreateCustomerComponent,
      },
      {
        path: 'EditCustomer/:id',
        component: EditCustomerComponent,
      },
      {
        path: 'CustomerList',
        component: CustomerListComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CustomerMasterRoutingModule {}
