import { Component } from '@angular/core';
import { CustomerInterface } from '../../../dashboard.interfaces';
import { CustomerMasterService } from '../customer-master.service';
import {AlertService} from "../../../../services/alert.service";

@Component({
  selector: 'app-customer-list',
  templateUrl: './customer-list.component.html',
  styleUrls: ['./customer-list.component.css'],
})
export class CustomerListComponent {
  customerData: CustomerInterface | any = [];
  deleteCustomerSuccess = false;
  noCustomerFound = false;
  constructor(private customerService: CustomerMasterService,
              private sweetAlert: AlertService) {
    this.getAllCustomer();
  }

  getAllCustomer() {
    this.customerService.getAllCustomer().subscribe((res) => {
      this.customerData = [];
      if(res.length > 0){
        this.customerData = res;
      } else {
        this.noCustomerFound = true;
        this.sweetAlert.alert('No Customer Found','Please Create Customer From Customer Master Section','warning');
      }



    });
  }

  deleteCustomer(id: number) {
    this.customerService.deleteCustomer(id).subscribe(() => {
      this.deleteCustomerSuccess = true;
      this.getAllCustomer();
    });
  }
}
