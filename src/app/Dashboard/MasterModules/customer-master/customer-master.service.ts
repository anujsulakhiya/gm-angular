import { Injectable } from '@angular/core';
import { HttpService } from '../../../services/http.service';
import {
  CreateCustomerInterface,
  CustomerInterface,
} from '../../dashboard.interfaces';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class CustomerMasterService {
  constructor(private httpService: HttpService) {}

  createCustomer(data: CreateCustomerInterface): Observable<CustomerInterface> {
    return this.httpService.post('/master/customer', data);
  }

  editCustomer(
    data: CreateCustomerInterface,
    id: number
  ): Observable<CustomerInterface> {
    return this.httpService.put('/master/customer/' + id, data);
  }

  getCustomer(id: number): Observable<CustomerInterface[]> {
    return this.httpService.get('/master/customer/' + id);
  }

  getAllCustomer(): Observable<CustomerInterface[]> {
    return this.httpService.get('/master/customer');
  }

  deleteCustomer(id: number): Observable<any> {
    return this.httpService.delete('/master/customer/' + id);
  }
}
