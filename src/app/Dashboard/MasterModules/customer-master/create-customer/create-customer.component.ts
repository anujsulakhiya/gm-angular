import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { CustomerMasterService } from '../customer-master.service';

@Component({
  selector: 'app-create-customer',
  templateUrl: './create-customer.component.html',
  styleUrls: ['./create-customer.component.css'],
})
export class CreateCustomerComponent {
  createCustomer: FormGroup | any;
  customerCreatedSuccess = false;
  existingCustomerName = false;

  constructor(
    private customerService: CustomerMasterService,
    private formBuilder: FormBuilder
  ) {
    this.initializeForm();
  }

  initializeForm() {
    this.createCustomer = this.formBuilder.group({
      customer_name: ['', [Validators.required]],
      customer_contact_no: ['', [Validators.nullValidator,Validators.pattern('^((\\+91-?)|0)?[0-9]{10}$')]],
      customer_address: ['', [Validators.required]],
      customer_gstin: ['', [Validators.nullValidator]],
    });
  }

  createCustomerMaster() {
    if (this.createCustomer.valid) {
      this.customerService.createCustomer(this.createCustomer.value).subscribe(
        () => {
          this.customerCreatedSuccess = true;
        },
        (err) => {
          if (err instanceof HttpErrorResponse) {
            if (err.error.errors.customer_name) {
              this.existingCustomerName = true;
            }
          }
        }
      );
    }
  }
}
