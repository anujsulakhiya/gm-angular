import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CustomerMasterRoutingModule } from './customer-master-routing.module';
import { CustomerMasterComponent } from './customer-master.component';
import { CreateCustomerComponent } from './create-customer/create-customer.component';
import { EditCustomerComponent } from './edit-customer/edit-customer.component';
import { CustomerListComponent } from './customer-list/customer-list.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    CustomerMasterComponent,
    CreateCustomerComponent,
    EditCustomerComponent,
    CustomerListComponent,
  ],
  imports: [CommonModule, CustomerMasterRoutingModule, ReactiveFormsModule],
})
export class CustomerMasterModule {}
