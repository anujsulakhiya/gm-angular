import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CustomerInterface } from '../../../dashboard.interfaces';
import { ActivatedRoute } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { CustomerMasterService } from '../customer-master.service';

@Component({
  selector: 'app-edit-customer',
  templateUrl: './edit-customer.component.html',
  styleUrls: ['./edit-customer.component.css'],
})
export class EditCustomerComponent {
  editCustomer: FormGroup | any;
  customerDetails: CustomerInterface | any = [];
  customerUpdateSuccess = false;
  existingCustomerName = false;
  customerId = 0;

  constructor(
    private customerService: CustomerMasterService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute
  ) {
    this.initializeForm(this.customerDetails);
    this.getRouteParam();
  }

  getRouteParam() {
    this.route.params.subscribe((params) => {
      this.customerId = params.id;
      this.getCustomerDetails(params.id);
    });
  }

  initializeForm(customerDetails: CustomerInterface) {
    this.editCustomer = this.formBuilder.group({
      customer_name: [customerDetails.customer_name, [Validators.required]],
      customer_contact_no: [
        customerDetails.customer_contact_no,
        [Validators.nullValidator,Validators.pattern('^((\\+91-?)|0)?[0-9]{10}$')],
      ],
      customer_address: [
        customerDetails.customer_address,
        [Validators.required],
      ],
      customer_gstin: [
        customerDetails.customer_gstin,
        [Validators.nullValidator],
      ],
    });
  }

  getCustomerDetails(id: number) {
    this.customerService.getCustomer(id).subscribe((res) => {
      this.customerDetails = res;
      this.initializeForm(this.customerDetails);
    });
  }

  editCustomerMaster() {

    this.customerUpdateSuccess = this.existingCustomerName =  false;
    if (this.editCustomer.valid) {
      this.customerService
        .editCustomer(this.editCustomer.value, this.customerId)
        .subscribe(
          (res) => {
            this.customerUpdateSuccess = true;
          },
          (err) => {
            if (err instanceof HttpErrorResponse) {
              if (err.error.errors.customer_name) {
                this.existingCustomerName = true;
              }
            }
          }
        );
    }
  }
}
