import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {BankMasterComponent} from "./bank-master.component";
import {CreateBankComponent} from "./create-bank/create-bank.component";
import {EditBankComponent} from "./edit-bank/edit-bank.component";
import {BankListComponent} from "./bank-list/bank-list.component";

const routes: Routes = [
  {
    path: '', component: BankMasterComponent, children: [
      {
        path: '', redirectTo: 'CreateBank', pathMatch: 'full'
      },
      {
        path: 'CreateBank', component: CreateBankComponent
      },
      {
        path: 'EditBank/:id', component: EditBankComponent
      },
      {
        path: 'BankList', component: BankListComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BankMasterRoutingModule { }
