import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BankMasterRoutingModule } from './bank-master-routing.module';
import { BankMasterComponent } from './bank-master.component';
import { CreateBankComponent } from './create-bank/create-bank.component';
import { EditBankComponent } from './edit-bank/edit-bank.component';
import { BankListComponent } from './bank-list/bank-list.component';
import {ReactiveFormsModule} from "@angular/forms";


@NgModule({
  declarations: [
    BankMasterComponent,
    CreateBankComponent,
    EditBankComponent,
    BankListComponent
  ],
  imports: [
    CommonModule,
    BankMasterRoutingModule,
    ReactiveFormsModule
  ]
})
export class BankMasterModule { }
