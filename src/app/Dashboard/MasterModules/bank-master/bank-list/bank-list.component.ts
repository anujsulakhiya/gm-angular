import { Component, OnInit } from '@angular/core';
import {BankInterface} from "../../../dashboard.interfaces";
import {BankMasterService} from "../bank-master.service";
import {AlertService} from "../../../../services/alert.service";

@Component({
  selector: 'app-bank-list',
  templateUrl: './bank-list.component.html',
  styleUrls: ['./bank-list.component.css']
})
export class BankListComponent {

  BankData: BankInterface|any = [];
  deleteBankSuccess = false;
  noBankFound = false;

  constructor(private BankService: BankMasterService,
              private sweetAlert: AlertService) {
    this.getAllBank();
  }

  getAllBank(){
    this.BankService.getAllBank().subscribe( res => {

      this.BankData = [];
      if(res.length > 0){
        this.BankData = res;
      } else {
        this.noBankFound = true;
        this.sweetAlert.alert('No Bank Found','Please Create Bank From Bank Master Section','warning');
      }
    })
  }

  deleteBank(id:number){
    this.BankService.deleteBank(id).subscribe( () => {
      this.deleteBankSuccess = true;
      this.getAllBank();
    })
  }
}
