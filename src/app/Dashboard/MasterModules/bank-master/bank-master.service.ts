import { Injectable } from '@angular/core';
import {HttpService} from "../../../services/http.service";
import {CreateBankInterface, BankInterface} from "../../dashboard.interfaces";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class BankMasterService {

  constructor(private httpService: HttpService) { }

  createBank(data: CreateBankInterface): Observable<BankInterface>{
    return this.httpService.post('/master/bank',data);
  }

  editBank(data: CreateBankInterface,id:number): Observable<BankInterface>{
    return this.httpService.put('/master/bank/'+id,data);
  }

  getBank(id:number): Observable<BankInterface[]>{
    return this.httpService.get('/master/bank/'+id);
  }

  getAllBank(): Observable<BankInterface[]>{
    return this.httpService.get('/master/bank');
  }

  deleteBank(id:number): Observable<any>{
    return this.httpService.delete('/master/bank/'+id);
  }

}
