import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {BankMasterService} from "../bank-master.service";
import {HttpErrorResponse} from "@angular/common/http";

@Component({
  selector: 'app-create-bank',
  templateUrl: './create-bank.component.html',
  styleUrls: ['./create-bank.component.css']
})
export class CreateBankComponent {

  createBank: FormGroup | any;
  BankCreatedSuccess = false;
  existingAccountNo = false;

  constructor(
    private bankService: BankMasterService,
    private formBuilder: FormBuilder
  ) {
    this.initializeForm();
  }

  initializeForm() {
    this.createBank = this.formBuilder.group({
      bank_name: ['', [Validators.required]],
      account_no: ['', [Validators.required]],
      ifsc_code: ['', [Validators.required]],
      branch: ['', [Validators.required]],
    });
  }

  createBankMaster() {
    this.BankCreatedSuccess = this.existingAccountNo = false;
    if (this.createBank.valid) {
      this.bankService.createBank(this.createBank.value).subscribe(
        () => {
          this.BankCreatedSuccess = true;
        },
        (err) => {
          if (err instanceof HttpErrorResponse) {
            if (err.error.errors.account_no) {
              this.existingAccountNo = true;
            }
          }
        }
      );
    }
  }

}
