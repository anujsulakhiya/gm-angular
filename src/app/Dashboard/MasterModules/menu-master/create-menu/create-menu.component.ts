import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MenuInterface } from 'src/app/Dashboard/dashboard.interfaces';
import { MenuService } from '../menu.service';

@Component({
  selector: 'app-create-menu',
  templateUrl: './create-menu.component.html',
  styleUrls: ['./create-menu.component.css']
})
export class CreateMenuComponent {

   
  createMenu: FormGroup | any;
  menuCreatedSuccess = false;
  existingMenuName = false;
  menuDetails: MenuInterface|any = []
  menuDetails2: MenuInterface|any = []

  constructor(
    private menuService: MenuService,
    private formBuilder: FormBuilder
  ) {
    this.initializeForm();
    this.getMenuCategory();
  }

  initializeForm() {
    this.createMenu = this.formBuilder.group({
      menu_category: ['', [Validators.required]],
      menu_name: ['', [Validators.required]],
    });
  }

  createMenuMaster() {
    this.menuCreatedSuccess = this.existingMenuName = false;
    if (this.createMenu.valid) {


      if(this.createMenu.value.menu_category.menu_category){
        this.createMenu.value.menu_category = this.createMenu.value.menu_category.menu_category;
      }
  
      
      if(this.createMenu.value.menu_name.menu_name){
        this.createMenu.value.menu_name = this.createMenu.value.menu_name.menu_name;
      }
      
      

      this.menuService.createMenu(this.createMenu.value).subscribe(
        () => {
          this.menuCreatedSuccess = true;
          this.getMenuCategory();
        },
        (err) => {
          if (err instanceof HttpErrorResponse) {
            if (err.error.errors.menu_name) {
              this.existingMenuName = true;
            }
          }
        }
      );
    }
  }

  getMenuCategory(){
    this.menuService.getAllMenu().subscribe( res => {
      this.menuDetails = res;

      this.menuDetails.map((item:any) => item.menu_category).filter((menu_category:any, index:any, self:any) => self.indexOf(menu_category) === index);


      this.menuDetails = this.menuDetails.filter((test:any, index:any, array:any) =>
        index === array.findIndex((findTest:any) =>
            findTest.menu_category === test.menu_category
        )
      );
      this.menuDetails2 = res;
    });
  }


    // ------------ Customer autocomplete ------------ //

    menuCategoryKeyword = 'menu_category';

    onChangeMenu(search: any) {
      // this.customerDetail = [];
      // this.customerIsSelected = false;
    }

    selectEventMenu(item: any) {
      this.menuDetails = item;
      // this.customerIsSelected = true;
    }
  
    onMenuCleared() {
     

    }
  
    menuFilter = function (
      menuDetails: any[],
      query: string
    ): MenuInterface[] {
      return  menuDetails.filter((x) => x.menu_category.toLowerCase().startsWith(query)
      );
    };

    menuNameKeyword = 'menu_name';

    onChangeMenuName(search: any) {
      // this.customerDetail = [];
      // this.customerIsSelected = false;
    }

    selectEventMenuName(item: any) {
      this.menuDetails2 = item;
      // this.customerIsSelected = true;
    }
  
    onMenuNameCleared() {
     

    }
  
    menuNameFilter = function (
      menuDetails2: any[],
      query: string
    ): MenuInterface[] {
      return  menuDetails2.filter((x) => x.menu_name.toLowerCase().startsWith(query)
      );
    };





}
