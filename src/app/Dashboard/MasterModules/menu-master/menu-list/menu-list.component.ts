import { Component, OnInit } from '@angular/core';
import { MenuInterface } from 'src/app/Dashboard/dashboard.interfaces';
import { MenuService } from '../menu.service';

@Component({
  selector: 'app-menu-list',
  templateUrl: './menu-list.component.html',
  styleUrls: ['./menu-list.component.css']
})
export class MenuListComponent {

  menuData: MenuInterface|any = [];
  deleteMenuSuccess = false;
  noMenuFound  = false;
  constructor(private menuService: MenuService) {
    this.getAllmenu();
  }

  getAllmenu(){
    this.menuService.getAllMenu().subscribe( res => {
      this.menuData = [];
      if(res.length > 0){
        this.menuData = res;
      } else {
        this.noMenuFound = true;
      }
    });
  }

  deletemenu(id:number){
    this.menuService.deleteMenu(id).subscribe( () => {
      this.deleteMenuSuccess = true;
      this.getAllmenu();
    });
  }

}
