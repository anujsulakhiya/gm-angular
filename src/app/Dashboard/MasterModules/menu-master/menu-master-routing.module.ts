import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateMenuComponent } from './create-menu/create-menu.component';
import { EditMenuComponent } from './edit-menu/edit-menu.component';
import { MenuListComponent } from './menu-list/menu-list.component';
import { MenuMasterComponent } from './menu-master.component';

const routes: Routes = [
  {
    path: '', component: MenuMasterComponent, children: [
      {
        path: '', redirectTo: 'CreateMenu', pathMatch: 'full'
      },
      {
        path: 'CreateMenu', component: CreateMenuComponent
      },
      {
        path: 'EditMenu/:id', component: EditMenuComponent
      },
      {
        path: 'MenuList', component: MenuListComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MenuMasterRoutingModule { }
