import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpService } from 'src/app/services/http.service';
import { MenuInterface } from '../../dashboard.interfaces';

@Injectable({
  providedIn: 'root'
})

export class MenuService {

  
  constructor(private httpService: HttpService) { }

  createMenu(data: MenuInterface): Observable<MenuInterface>{
    return this.httpService.post('/master/menu',data);
  }

  editMenu(data: MenuInterface,id:number): Observable<MenuInterface>{
    return this.httpService.put('/master/menu/'+id,data);
  }

  getMenu(id:number): Observable<MenuInterface[]>{
    return this.httpService.get('/master/menu/'+id);
  }

  getAllMenu(): Observable<MenuInterface[]>{
    return this.httpService.get('/master/menu');
  }

  deleteMenu(id:number): Observable<any>{
    return this.httpService.delete('/master/menu/'+id);
  }

}
