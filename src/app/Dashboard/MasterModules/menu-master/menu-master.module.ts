import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MenuMasterRoutingModule } from './menu-master-routing.module';
import { MenuMasterComponent } from './menu-master.component';
import { CreateMenuComponent } from './create-menu/create-menu.component';
import { ReactiveFormsModule } from '@angular/forms';
import { EditMenuComponent } from './edit-menu/edit-menu.component';
import { MenuListComponent } from './menu-list/menu-list.component';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';


@NgModule({
  declarations: [
    MenuMasterComponent,
    CreateMenuComponent,
    EditMenuComponent,
    MenuListComponent
  ],
  imports: [
    CommonModule,
    MenuMasterRoutingModule,
    ReactiveFormsModule,
    AutocompleteLibModule
  ]
})
export class MenuMasterModule { }
