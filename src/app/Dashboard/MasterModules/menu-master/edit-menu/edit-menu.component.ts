import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { MenuInterface } from 'src/app/Dashboard/dashboard.interfaces';
import { MenuService } from '../menu.service';

@Component({
  selector: 'app-edit-menu',
  templateUrl: './edit-menu.component.html',
  styleUrls: ['./edit-menu.component.css']
})
export class EditMenuComponent {

  editMenu: FormGroup | any;
  menuDetails: MenuInterface | any = [];
  menuUpdateSuccess = false;
  existingMenuName = false;
  menuId = 0;

  constructor(
    private menuService: MenuService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute
  ) {
    this.initializeForm(this.menuDetails);
    this.getRouteParam();
  }

  getRouteParam() {
    this.route.params.subscribe((params) => {
      this.menuId = params.id;
      this.getMenuDetails(params.id);
    });
  }

  initializeForm(menuDetails: MenuInterface) {
    this.editMenu = this.formBuilder.group({
      menu_category: [menuDetails.menu_category, [Validators.required]],
      menu_name: [menuDetails.menu_name, [Validators.required]],
    });
  }

  getMenuDetails(id: number) {
    this.menuService.getMenu(id).subscribe((res) => {
      this.menuDetails = res;
      this.initializeForm(this.menuDetails);
    });
  }

  editMenuMaster() {
    this.existingMenuName = this.menuUpdateSuccess = false;
    if (this.editMenu.valid) {
      this.menuService.editMenu(this.editMenu.value, this.menuId).subscribe(
        (res) => {
          this.menuUpdateSuccess = true;
        },
        (err) => {
          if (err instanceof HttpErrorResponse) {
            if (err.error.errors.menu_name) {
              this.existingMenuName = true;
            }
          }
        }
      );
    }
  }

}
