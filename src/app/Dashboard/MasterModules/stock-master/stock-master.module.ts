import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {StockMasterRoutingModule} from './stock-master-routing.module';
import {CreateStockComponent} from './create-stock/create-stock.component';
import {EditStockComponent} from './edit-stock/edit-stock.component';
import {StockListComponent} from './stock-list/stock-list.component';
import {ReactiveFormsModule} from '@angular/forms';


@NgModule({
  declarations: [
    CreateStockComponent,
    EditStockComponent,
    StockListComponent
  ],
    imports: [
        CommonModule,
        StockMasterRoutingModule,
        ReactiveFormsModule
    ]
})

export class StockMasterModule { }
