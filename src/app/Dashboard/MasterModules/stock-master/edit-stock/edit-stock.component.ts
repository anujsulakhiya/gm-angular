import {Component} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {FirmInterface, StockInterface} from '../../../dashboard.interfaces';
import {StockMasterService} from '../stock-master.service';
import {FirmMasterService} from '../../firm-master/firm-master.service';
import {HttpErrorResponse} from '@angular/common/http';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-edit-stock',
  templateUrl: './edit-stock.component.html',
  styleUrls: ['./edit-stock.component.css']
})
export class EditStockComponent {

  editStock: FormGroup|any;
  stockDetails: StockInterface|any = [];
  stockEditSuccess = false;
  existingStockName = false;
  firmData: FirmInterface|any = [];
  isInventoryValue = 0;
  isInventoryTrue = false;
  stockId = 0;

  constructor(private stockService: StockMasterService,
              private firmService: FirmMasterService,
              private formBuilder: FormBuilder,
              private route: ActivatedRoute) {
    this.initializeForm(this.stockDetails);
    this.getRouteParam();
    this.getAllFirm();
  }

  getRouteParam() {
    this.route.params.subscribe(params => {
      this.stockId = params.id;
      this.getStockDetails(params.id);
    });
  }

  initializeForm(stockDetails: StockInterface) {
    this.editStock = this.formBuilder.group({
      firm_id: [stockDetails.firm_id, [Validators.required]],
      stock_name: [stockDetails.stock_name, [Validators.required]],
      stock_mrp: [stockDetails.stock_mrp, [Validators.required]],
      stock_qty: [stockDetails.stock_qty, [Validators.nullValidator]],
      stock_gst: [stockDetails.stock_gst, [Validators.nullValidator]],
      is_inventory: [stockDetails.is_inventory, [Validators.required]],
      stock_type: [stockDetails.stock_type, [Validators.required]],
      stock_value: [stockDetails.stock_value, [Validators.nullValidator]],
    });
  }

  getStockDetails(id:number){
    this.stockService.getStock(id).subscribe( res => {
      this.stockDetails = res;
      this.isInventoryTrue = this.stockDetails.isInventory != 0;
      this.initializeForm(this.stockDetails);
    })
  }

  isInventory(event:any){
    this.isInventoryTrue = event.target.value == 1;
  }

  getAllFirm(){
    this.firmService.getAllFirm().subscribe( res => {
      this.firmData = res;
    })
  }

  editStockMaster(){
    this.stockEditSuccess = this.existingStockName = false;
    if(this.editStock.valid){
      if(!this.isInventoryTrue){
        this.editStock.value.stock_value = 0;
      }
      this.stockService.editStock(this.editStock.value,this.stockId).subscribe( () => {
        this.stockEditSuccess = true;
      }, (err) => {
        if (err instanceof HttpErrorResponse) {

          if(err.error.errors.stock_name){
            this.existingStockName = true;
          }
        }
      });
    }

  }

}
