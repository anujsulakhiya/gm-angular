import {Component} from '@angular/core';
import {StockInterface} from '../../../dashboard.interfaces';
import {StockMasterService} from '../stock-master.service';
import {AlertService} from "../../../../services/alert.service";

@Component({
  selector: 'app-stock-list',
  templateUrl: './stock-list.component.html',
  styleUrls: ['./stock-list.component.css']
})
export class StockListComponent {


  stockData: StockInterface|any = [];
  deleteStockSuccess = false;
  noStockFound = false;

  constructor(private stockService: StockMasterService,
              private sweetAlert: AlertService) {
    this.getAllFirm();
  }

  getAllFirm(){
    this.stockService.getAllStock().subscribe( res => {
      this.stockData = [];
      if(res.length > 0){
        this.stockData = res;
      } else {
        this.noStockFound = true;
        this.sweetAlert.alert('No Stock Created Yet','Create Stock From Stock Master Section','warning');

      }

    })
  }

  deleteStock(id:number){
    this.stockService.deleteStock(id).subscribe( () => {
      this.deleteStockSuccess = true;
      this.getAllFirm();
    })
  }


}
