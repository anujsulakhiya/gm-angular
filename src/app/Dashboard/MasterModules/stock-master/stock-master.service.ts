import { Injectable } from '@angular/core';
import { HttpService } from '../../../services/http.service';
import {
  CreateStockInterface,
  StockInterface,
} from '../../dashboard.interfaces';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class StockMasterService {
  constructor(private httpService: HttpService) {}

  createStock(data: CreateStockInterface): Observable<StockInterface> {
    return this.httpService.post('/master/stock', data);
  }

  editStock(
    data: CreateStockInterface,
    id: number
  ): Observable<StockInterface> {
    return this.httpService.put('/master/stock/' + id, data);
  }

  getStock(id: number): Observable<StockInterface[]> {
    return this.httpService.get('/master/stock/' + id);
  }

  getAllStock(): Observable<StockInterface[]> {
    return this.httpService.get('/master/stock');
  }

  deleteStock(id: number): Observable<any> {
    return this.httpService.delete('/master/stock/' + id);
  }
}
