import {Component} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {HttpErrorResponse} from '@angular/common/http';
import {StockMasterService} from '../stock-master.service';
import {FirmMasterService} from '../../firm-master/firm-master.service';
import {FirmInterface} from '../../../dashboard.interfaces';
import {AlertService} from "../../../../services/alert.service";

@Component({
  selector: 'app-create-stock',
  templateUrl: './create-stock.component.html',
  styleUrls: ['./create-stock.component.css']
})
export class CreateStockComponent {

  createStock: FormGroup|any;
  stockCreatedSuccess = false;
  existingStockName = false;
  firmData: FirmInterface|any = [];
  isInventoryTrue = false;

  constructor(private stockService: StockMasterService,
              private firmService: FirmMasterService,
              private formBuilder: FormBuilder,
              private sweetAlert: AlertService) {
    this.initializeForm();
    this.getAllFirm();
  }

  initializeForm() {
    this.createStock = this.formBuilder.group({
      firm_id: ['', [Validators.required]],
      stock_name: ['', [Validators.required]],
      stock_mrp: ['', [Validators.required]],
      stock_qty: [1, [Validators.nullValidator]],
      stock_gst: ['', [Validators.nullValidator]],
      is_inventory: ['', [Validators.required]],
      stock_type: [0, [Validators.required]],
      stock_value: ['', [Validators.nullValidator]],
    });
  }

  isInventory(event:any){

    this.isInventoryTrue = event.target.value == 1;

  }

  getAllFirm(){
    this.firmService.getAllFirm().subscribe( res => {



      if(res.length > 0){
        this.firmData = res;
      } else  {
        this.sweetAlert.alert('No Firm Found','Please Create Firm From Firm Master Section','warning');
      }
    })
  }

  createStockMaster(){
    this.stockCreatedSuccess = this.existingStockName = false;
    if(this.createStock.valid){
      if(!this.isInventoryTrue){
        this.createStock.value.stock_value = 0;
      }
      this.stockService.createStock(this.createStock.value).subscribe( () => {
        this.stockCreatedSuccess = true;
        this.initializeForm();
      }, (err) => {
        if (err instanceof HttpErrorResponse) {

          if(err.error.errors.stock_name){
            this.existingStockName = true;
          }
        }
      });
    }

  }

}
