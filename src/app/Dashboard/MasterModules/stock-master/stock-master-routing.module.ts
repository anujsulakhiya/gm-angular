import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {StockMasterComponent} from './stock-master.component';
import {CreateStockComponent} from './create-stock/create-stock.component';
import {EditStockComponent} from './edit-stock/edit-stock.component';
import {StockListComponent} from './stock-list/stock-list.component';

const routes: Routes = [
  {
    path: '', component: StockMasterComponent, children: [
      {
        path: '', redirectTo: 'StockList', pathMatch: 'full'
      },
      {
        path: 'CreateStock', component: CreateStockComponent
      },
      {
        path: 'EditStock/:id', component: EditStockComponent
      },
      {
        path: 'StockList', component: StockListComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StockMasterRoutingModule { }
