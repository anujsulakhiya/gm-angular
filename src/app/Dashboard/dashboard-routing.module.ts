import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { ChangePasswordComponent } from './Components/change-password/change-password.component';
import { HomeComponent } from './Components/home/home.component';
import { UserProfileComponent } from './Components/user-profile/user-profile.component';
import { InvoiceFormateComponent } from './Modules/customer-ledger/invoice-formate/invoice-formate.component';

const routes: Routes = [
  {
    path: '', component: DashboardComponent, children: [
      {
        path: '', redirectTo: 'Home', pathMatch: 'full'
      },
      {
        path: 'Home', component: HomeComponent
      },
      {
        path: 'Profile', component: UserProfileComponent
      },
      {
        path: 'ChangePassword', component: ChangePasswordComponent
      },    
    
      {
        path: 'Masters',
        loadChildren: () => import('./MasterModules/master.module').then(m => m.MasterModule),
      },
      {
        path: 'Booking',
        loadChildren: () => import('./Modules/booking/booking.module').then(m => m.BookingModule),
      },
      {
        path: 'StockReturn',
        loadChildren: () => import('./Modules/stock-return/stock-return.module').then(m => m.StockReturnModule),
      },
      {
        path: 'CustomerLedger',
        loadChildren: () => import('./Modules/customer-ledger/customer-ledger.module').then(m => m.CustomerLedgerModule),
      },      
      {
        path: 'CustomerMenu',
        loadChildren: () => import('./Modules/customer-menu/customer-menu.module').then(m => m.CustomerMenuModule),
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule {
}
