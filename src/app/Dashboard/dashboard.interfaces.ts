export interface CreateFirmInterface {
  firm_name: string;
  firm_contact_no: number;
  firm_address: string;
  firm_gstin: string;
}

export interface FirmInterface {
  id: number;
  firm_name: string;
  firm_contact_no: number;
  firm_address: string;
  firm_gstin: string;
  created_at: string;
}

export interface MenuInterface {
  id?: number,
  menu_category: string,
  menu_name: string,
  created_at?: string,
}

export interface CreateStockInterface {
  firm_id: number;
  stock_name: string;
  stock_mrp: number;
  stock_gst: number;
  stock_type: boolean;
  is_inventory: boolean;
  stock_value: number;
}

export interface StockInterface {
  id?: number;
  firm_id: number;
  stock_name: string;
  stock_mrp: number;
  stock_qty: number;
  stock_available?: number;
  stock_gst: number;
  stock_type: boolean;
  is_inventory: number;
  stock_value: number;
  created_at?: string;
}

export interface CreateBookingInterface {
  invoice_id: number;
  invoice_date: string;
  customer_name: string;
  customer_address: string;
  customer_contact_no: number;
  customer_gstin: number;
  service_id: number;
  rate: number;
  book_qty: number;
  date: string;
  days: number;
  time: string;
}

export interface CreateCustomerInterface {
  customer_name: string;
  customer_address: string;
  customer_contact_no: number;
  customer_gstin: number;
}

export interface CustomerInterface {
  id?: number;
  customer_name: string;
  customer_address: string;
  customer_contact_no: number;
  customer_gstin: number;
}
export interface CreateBankInterface {
  bank_name: string;
  account_no: number;
  ifsc_code: string;
  branch: string;
}

export interface BankInterface {
  id?: number;
  bank_name: string;
  account_no: number;
  ifsc_code: string;
  branch: string;
}

export interface BookingInterface {
  id: number;
  type?: number;
  is_inventory?: number;
  invoice_number: number;
  invoice_id: number;
  invoice_date?: string;
  customer_name: string;
  customer_address: string;
  customer_contact_no: number;
  customer_gstin: number;
  service_id?: number;
  stock_name: string;
  stock_value?: string;
  rate: number;
  extra_rate?: number;
  qty_in?: number;
  qty_out?: number;
  qty_remaining?: number;
  date: string;
  days: number;
  time: string;
  total?: number;
  created_at?: string;
}

export interface InvoiceInterface {
  id: number;
  invoice_customer_id: number;
  invoice_date: string;
  invoice_number: string;
  invoice_total: number;
  paid_amount: number;
  discount: number;
  debit: number;
  total_remaining_qty: number;
  created_at: string;
}

export interface InvoiceNumberInterface {
  invoice_id?: number;
  invoice_number: string;
  is_inventory?: number;
  type?: number
}

export interface LatestInvoiceInterface {
  id: number;
  invoice_number: number;
}


export interface StockReturnInterface {
  id?: number;
  type?: number;
  invoice_number: number;
  invoice_id: number;
  invoice_date?: string;
  customer_name: string;
  customer_address: string;
  customer_contact_no: number;
  customer_gstin: number;
  service_id: number;
  stock_name: string;
  rate: number;
  extra_rate?: number;
  qty_in: number;
  qty_out?: number;
  qty_lost?: number;
  lost_value?: number;
  date: string;
  days: number;
  time: string;
  total?: number;
  created_at?: string;
}

export interface invoiceCustomerId {
  invoice_customer_id?: number;
}
export interface GetLedgerInterface {
  customer_id?: number;
  from_date?:string;
  to_date?:string;
}







