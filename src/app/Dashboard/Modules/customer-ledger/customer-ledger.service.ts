import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {HttpService} from "../../../services/http.service";
import {GetLedgerInterface, invoiceCustomerId, InvoiceInterface} from "../../dashboard.interfaces";

@Injectable({
  providedIn: 'root'
})
export class CustomerLedgerService {

  constructor(private httpService: HttpService) { }


  getCustomerLedger(id:number): Observable<any>{
    return this.httpService.get('/customerLedgerReport/'+id);
  }

  getInvoice(invoice_id:number): Observable<any>{
    return this.httpService.get('/getInvoiceOfAllFirm/'+invoice_id);
  }


  getCustomerLedgerWithDate(customer_id:number,ledger: GetLedgerInterface): Observable<any>{
    return this.httpService.getWithParams('/customerLedgerReport/'+customer_id,ledger );
  }

  getLatestReceiptNo(): Observable<any>{
    return this.httpService.get('/getLatestReceiptNo');
  }
  getCustomerInvoice(customerId:number): Observable<InvoiceInterface[]>{
    return this.httpService.get('/getActiveInvoice/'+customerId);
  }

  getSingleCustomerLedger(ledgerId:number): Observable<InvoiceInterface[]>{
    return this.httpService.get('/customer_ledger/'+ledgerId);
  }

  createLedger(data: any):Observable<any>{
    return this.httpService.post('/customer_ledger',data);
  }
  editLedger(ledgerId:number,data: any):Observable<any>{
    return this.httpService.put('/customer_ledger/'+ledgerId,data);
  }

  deleteLedger(ledgerId:number):Observable<any>{
    return this.httpService.delete('/customer_ledger/'+ledgerId);
  }



}
