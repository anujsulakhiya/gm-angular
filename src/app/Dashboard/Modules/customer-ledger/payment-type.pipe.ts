import { Pipe, PipeTransform } from '@angular/core';

const paymentType: string[] = ['Credit', 'Debit'];

@Pipe({
  name: 'paymentType'
})
class PaymentTypePipe implements PipeTransform {

  transform(value: number): string {
    return paymentType[value];
  }

}

export {PaymentTypePipe, paymentType}
