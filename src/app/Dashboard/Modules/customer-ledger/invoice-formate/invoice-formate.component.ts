import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { StockMasterService } from 'src/app/Dashboard/MasterModules/stock-master/stock-master.service';
import { DashboardService } from '../../../dashboard.service';
import { CustomerLedgerService } from '../customer-ledger.service';


@Component({
  selector: 'app-invoice-formate',
  templateUrl: './invoice-formate.component.html',
  styleUrls: ['./invoice-formate.component.css'],
  // schemas: [NO_ERRORS_SCHEMA]
})
export class InvoiceFormateComponent {


  invoiceData:any = [];
  routeParams = false;
  invoiceId : any;
  invoiceTotal =0;
  stockData:any;
  stockDataObject:any = [];

  @ViewChild('invoice_print') invoice_print: any;

  constructor(private ledgerService: CustomerLedgerService,
    private route: ActivatedRoute,
    private stockService: StockMasterService ) {

      this.getRouteParam();
      this.getAllStock();


  }

  css2 = `/*! CSS Used from: http://127.0.0.1:4200/assets/vendor/@fortawesome/fontawesome-free/css/all.min.css */
  .fa{-moz-osx-font-smoothing:grayscale;-webkit-font-smoothing:antialiased;display:inline-block;font-style:normal;font-variant:normal;text-rendering:auto;line-height:1;}
  .fa-circle:before{content:"\f111";}
  .fa{font-family:"Font Awesome 5 Free";}
  .fa{font-weight:900;}
  /*! CSS Used from: https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/fontawesome.min.css */
  .fa{font-family:"Font Awesome 6 Free";font-family:var(--fa-style-family,"Font Awesome 6 Free");font-weight:900;font-weight:var(--fa-style,900);}
  .fa{-moz-osx-font-smoothing:grayscale;-webkit-font-smoothing:antialiased;display:inline-block;display:var(--fa-display,inline-block);font-style:normal;font-variant:normal;line-height:1;text-rendering:auto;}
  .fa-circle:before{content:"\f111";}
  /*! CSS Used from: http://127.0.0.1:4200/styles.css */
  .mat-typography h1{font:400 24px/32px Roboto, "Helvetica Neue", sans-serif;letter-spacing:normal;margin:0 0 16px;}
  .mat-typography{font:400 14px/20px Roboto, "Helvetica Neue", sans-serif;letter-spacing:normal;}
  *,*::before,*::after{box-sizing:border-box;}
  body{margin:0;font-family:Open Sans, sans-serif;font-size:1rem;font-weight:400;line-height:1.5;color:#525f7f;text-align:left;background-color:#f8f9fe;}
  hr{box-sizing:content-box;height:0;overflow:visible;}
  h1{margin-top:0;margin-bottom:0.5rem;}
  b{font-weight:bolder;}
  button{border-radius:0;}
  button:focus{outline:1px dotted;outline:5px auto -webkit-focus-ring-color;}
  button{margin:0;font-family:inherit;font-size:inherit;line-height:inherit;}
  button{overflow:visible;}
  button{text-transform:none;}
  button{-webkit-appearance:button;}
  button::-moz-focus-inner{padding:0;border-style:none;}
  [hidden]{display:none!important;}
  h1{margin-bottom:0.5rem;font-family:inherit;font-weight:600;line-height:1.5;color:#32325d;}
  h1{font-size:1.625rem;}
  hr{margin-top:2rem;margin-bottom:2rem;border:0;border-top:1px solid rgba(0, 0, 0, 0.1);}
  .container{width:100%;padding-right:15px;padding-left:15px;margin-right:auto;margin-left:auto;}
  @media (min-width: 576px){
  .container{max-width:540px;}
  }
  @media (min-width: 768px){
  .container{max-width:720px;}
  }
  @media (min-width: 992px){
  .container{max-width:960px;}
  }
  @media (min-width: 1200px){
  .container{max-width:1140px;}
  }
  .row{display:flex;flex-wrap:wrap;margin-right:-15px;margin-left:-15px;}
  .col-md-12,.col-sm-7,.col-sm-6,.col-sm-5,.col-12,.col-7,.col-6,.col-5,.col-1{position:relative;width:100%;padding-right:15px;padding-left:15px;}
  .col-1{flex:0 0 8.3333333333%;max-width:8.3333333333%;}
  .col-5{flex:0 0 41.6666666667%;max-width:41.6666666667%;}
  .col-6{flex:0 0 50%;max-width:50%;}
  .col-7{flex:0 0 58.3333333333%;max-width:58.3333333333%;}
  .col-12{flex:0 0 100%;max-width:100%;}
  .order-first{order:-1;}
  @media (min-width: 576px){
  .col-sm-5{flex:0 0 41.6666666667%;max-width:41.6666666667%;}
  .col-sm-6{flex:0 0 50%;max-width:50%;}
  .col-sm-7{flex:0 0 58.3333333333%;max-width:58.3333333333%;}
  .order-sm-last{order:13;}
  }
  @media (min-width: 768px){
  .col-md-12{flex:0 0 100%;max-width:100%;}
  }
  .align-middle{vertical-align:middle!important;}
  @media (min-width: 576px){
  .d-sm-none{display:none!important;}
  .d-sm-flex{display:flex!important;}
  }
  .justify-content-end{justify-content:flex-end!important;}
  .align-items-center{align-items:center!important;}
  .align-self-start{align-self:flex-start!important;}
  .mt-1,.my-1{margin-top:0.25rem!important;}
  .mr-1{margin-right:0.25rem!important;}
  .my-1{margin-bottom:0.25rem!important;}
  .mt-2,.my-2{margin-top:0.5rem!important;}
  .mx-2{margin-right:0.5rem!important;}
  .my-2{margin-bottom:0.5rem!important;}
  .mx-2{margin-left:0.5rem!important;}
  .mt-3{margin-top:1rem!important;}
  .mt-4{margin-top:1.5rem!important;}
  .mt-5{margin-top:3rem!important;}
  .px-0{padding-right:0!important;}
  .px-0{padding-left:0!important;}
  .p-2{padding:0.5rem!important;}
  .mx-n1{margin-right:-0.25rem!important;}
  .mx-n1{margin-left:-0.25rem!important;}
  @media (min-width: 576px){
  .mb-sm-0{margin-bottom:0!important;}
  }
  @media (min-width: 992px){
  .mt-lg-0{margin-top:0!important;}
  }
  .text-right{text-align:right!important;}
  .text-center{text-align:center!important;}
  .text-capitalize{text-transform:capitalize!important;}
  .text-white{color:#fff!important;}
  .text-white{color:#fff!important;}
  @media print{
  *,*::before,*::after{text-shadow:none!important;box-shadow:none!important;}
  body{min-width:992px!important;}
  .container{min-width:992px!important;}
  }
  .text-xs{font-size:0.75rem!important;}
  .text-sm{font-size:0.875rem!important;}
  .text-blue{color:#5e72e4!important;}
  .text-white{color:#fff!important;}
  body{height:100%;}
  body{margin:0;font-family:Roboto, "Helvetica Neue", sans-serif;}
  /*! CSS Used from: Embedded */
  .loading-spinner[_ngcontent-wly-c18]{background-color:#0000001f;position:absolute;width:100%;top:0px;left:0px;height:100%;align-items:center;justify-content:center;display:grid;}
  .lds-roller[_ngcontent-wly-c18]{display:inline-block;position:relative;width:80px;height:80px;}
  .lds-roller[_ngcontent-wly-c18]   div[_ngcontent-wly-c18]{animation:lds-roller 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;transform-origin:40px 40px;}
  .lds-roller[_ngcontent-wly-c18]   div[_ngcontent-wly-c18]:after{content:" ";display:block;position:absolute;width:7px;height:7px;border-radius:50%;background:#fff;margin:-4px 0 0 -4px;}
  .lds-roller[_ngcontent-wly-c18]   div[_ngcontent-wly-c18]:nth-child(1){animation-delay:-0.036s;}
  .lds-roller[_ngcontent-wly-c18]   div[_ngcontent-wly-c18]:nth-child(1):after{top:63px;left:63px;}
  .lds-roller[_ngcontent-wly-c18]   div[_ngcontent-wly-c18]:nth-child(2){animation-delay:-0.072s;}
  .lds-roller[_ngcontent-wly-c18]   div[_ngcontent-wly-c18]:nth-child(2):after{top:68px;left:56px;}
  .lds-roller[_ngcontent-wly-c18]   div[_ngcontent-wly-c18]:nth-child(3){animation-delay:-0.108s;}
  .lds-roller[_ngcontent-wly-c18]   div[_ngcontent-wly-c18]:nth-child(3):after{top:71px;left:48px;}
  .lds-roller[_ngcontent-wly-c18]   div[_ngcontent-wly-c18]:nth-child(4){animation-delay:-0.144s;}
  .lds-roller[_ngcontent-wly-c18]   div[_ngcontent-wly-c18]:nth-child(4):after{top:72px;left:40px;}
  .lds-roller[_ngcontent-wly-c18]   div[_ngcontent-wly-c18]:nth-child(5){animation-delay:-0.18s;}
  .lds-roller[_ngcontent-wly-c18]   div[_ngcontent-wly-c18]:nth-child(5):after{top:71px;left:32px;}
  .lds-roller[_ngcontent-wly-c18]   div[_ngcontent-wly-c18]:nth-child(6){animation-delay:-0.216s;}
  .lds-roller[_ngcontent-wly-c18]   div[_ngcontent-wly-c18]:nth-child(6):after{top:68px;left:24px;}
  .lds-roller[_ngcontent-wly-c18]   div[_ngcontent-wly-c18]:nth-child(7){animation-delay:-0.252s;}
  .lds-roller[_ngcontent-wly-c18]   div[_ngcontent-wly-c18]:nth-child(7):after{top:63px;left:17px;}
  .lds-roller[_ngcontent-wly-c18]   div[_ngcontent-wly-c18]:nth-child(8){animation-delay:-0.288s;}
  .lds-roller[_ngcontent-wly-c18]   div[_ngcontent-wly-c18]:nth-child(8):after{top:56px;left:12px;}
  /*! CSS Used from: Embedded */
  .text-secondary-d1[_ngcontent-wly-c17]{color:#728299!important;}
  .brc-default-l1[_ngcontent-wly-c17]{border-color:#dce9f0!important;}
  .mx-n1[_ngcontent-wly-c17]{margin-left:-.25rem!important;}
  .mx-n1[_ngcontent-wly-c17]{margin-right:-.25rem!important;}
  hr[_ngcontent-wly-c17]{margin-top:1rem;margin-bottom:1rem;border:0;border-top:1px solid rgba(0,0,0,.1);}
  .text-grey-m2[_ngcontent-wly-c17]{color:#888a8d!important;}
  .text-600[_ngcontent-wly-c17]{font-weight:600!important;}
  .text-110[_ngcontent-wly-c17]{font-size:110%!important;}
  .text-blue[_ngcontent-wly-c17]{color:#478fcc!important;}
  .py-25[_ngcontent-wly-c17]{padding-bottom:.75rem!important;}
  .py-25[_ngcontent-wly-c17]{padding-top:.75rem!important;}
  .bgc-default-tp1[_ngcontent-wly-c17]{background-color:rgba(121,169,197,.92)!important;}
  .text-120[_ngcontent-wly-c17]{font-size:120%!important;}
  .text-blue-m2[_ngcontent-wly-c17]{color:#68a3d5!important;}
  .text-150[_ngcontent-wly-c17]{font-size:150%!important;}
  .page[_ngcontent-wly-c17]{background:white;display:block;margin:0 auto;margin-bottom:0.5cm;box-shadow:0 0 0.5cm rgba(0,0,0,0.5);}
  .page[size="A4"][_ngcontent-wly-c17]{width:21cm;height:29.7cm;}
  @media print{
  .page[_ngcontent-wly-c17]{margin:0;box-shadow:0;}
  }
  /*! CSS Used from: https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css */
  .fa{display:inline-block;font:normal normal normal 14px/1 FontAwesome;font-size:inherit;text-rendering:auto;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale;}
  .fa-circle:before{content:"\f111";}
  /*! CSS Used keyframes */
  @keyframes lds-roller{0%{transform:rotate(0deg);}100%{transform:rotate(360deg);}}
  /*! CSS Used fontfaces */
  @font-face{font-family:"Font Awesome 5 Free";font-style:normal;font-weight:400;src:url(http://127.0.0.1:4200/assets/vendor/@fortawesome/fontawesome-free/webfonts/fa-regular-400.eot);src:url(http://127.0.0.1:4200/assets/vendor/@fortawesome/fontawesome-free/webfonts/fa-regular-400.eot#iefix) format("embedded-opentype"),url(http://127.0.0.1:4200/assets/vendor/@fortawesome/fontawesome-free/webfonts/fa-regular-400.woff2) format("woff2"),url(http://127.0.0.1:4200/assets/vendor/@fortawesome/fontawesome-free/webfonts/fa-regular-400.woff) format("woff"),url(http://127.0.0.1:4200/assets/vendor/@fortawesome/fontawesome-free/webfonts/fa-regular-400.ttf) format("truetype"),url(http://127.0.0.1:4200/assets/vendor/@fortawesome/fontawesome-free/webfonts/fa-regular-400.svg#fontawesome) format("svg");}
  @font-face{font-family:"Font Awesome 5 Free";font-style:normal;font-weight:900;src:url(http://127.0.0.1:4200/assets/vendor/@fortawesome/fontawesome-free/webfonts/fa-solid-900.eot);src:url(http://127.0.0.1:4200/assets/vendor/@fortawesome/fontawesome-free/webfonts/fa-solid-900.eot#iefix) format("embedded-opentype"),url(http://127.0.0.1:4200/assets/vendor/@fortawesome/fontawesome-free/webfonts/fa-solid-900.woff2) format("woff2"),url(http://127.0.0.1:4200/assets/vendor/@fortawesome/fontawesome-free/webfonts/fa-solid-900.woff) format("woff"),url(http://127.0.0.1:4200/assets/vendor/@fortawesome/fontawesome-free/webfonts/fa-solid-900.ttf) format("truetype"),url(http://127.0.0.1:4200/assets/vendor/@fortawesome/fontawesome-free/webfonts/fa-solid-900.svg#fontawesome) format("svg");}
  @font-face{font-family:'Roboto';font-style:normal;font-weight:300;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v29/KFOlCnqEu92Fr1MmSU5fCRc4EsA.woff2) format('woff2');unicode-range:U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;}
  @font-face{font-family:'Roboto';font-style:normal;font-weight:300;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v29/KFOlCnqEu92Fr1MmSU5fABc4EsA.woff2) format('woff2');unicode-range:U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;}
  @font-face{font-family:'Roboto';font-style:normal;font-weight:300;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v29/KFOlCnqEu92Fr1MmSU5fCBc4EsA.woff2) format('woff2');unicode-range:U+1F00-1FFF;}
  @font-face{font-family:'Roboto';font-style:normal;font-weight:300;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v29/KFOlCnqEu92Fr1MmSU5fBxc4EsA.woff2) format('woff2');unicode-range:U+0370-03FF;}
  @font-face{font-family:'Roboto';font-style:normal;font-weight:300;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v29/KFOlCnqEu92Fr1MmSU5fCxc4EsA.woff2) format('woff2');unicode-range:U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;}
  @font-face{font-family:'Roboto';font-style:normal;font-weight:300;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v29/KFOlCnqEu92Fr1MmSU5fChc4EsA.woff2) format('woff2');unicode-range:U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;}
  @font-face{font-family:'Roboto';font-style:normal;font-weight:300;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v29/KFOlCnqEu92Fr1MmSU5fBBc4.woff2) format('woff2');unicode-range:U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;}
  @font-face{font-family:'Roboto';font-style:normal;font-weight:400;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v29/KFOmCnqEu92Fr1Mu72xKOzY.woff2) format('woff2');unicode-range:U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;}
  @font-face{font-family:'Roboto';font-style:normal;font-weight:400;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v29/KFOmCnqEu92Fr1Mu5mxKOzY.woff2) format('woff2');unicode-range:U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;}
  @font-face{font-family:'Roboto';font-style:normal;font-weight:400;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v29/KFOmCnqEu92Fr1Mu7mxKOzY.woff2) format('woff2');unicode-range:U+1F00-1FFF;}
  @font-face{font-family:'Roboto';font-style:normal;font-weight:400;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v29/KFOmCnqEu92Fr1Mu4WxKOzY.woff2) format('woff2');unicode-range:U+0370-03FF;}
  @font-face{font-family:'Roboto';font-style:normal;font-weight:400;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v29/KFOmCnqEu92Fr1Mu7WxKOzY.woff2) format('woff2');unicode-range:U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;}
  @font-face{font-family:'Roboto';font-style:normal;font-weight:400;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v29/KFOmCnqEu92Fr1Mu7GxKOzY.woff2) format('woff2');unicode-range:U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;}
  @font-face{font-family:'Roboto';font-style:normal;font-weight:400;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v29/KFOmCnqEu92Fr1Mu4mxK.woff2) format('woff2');unicode-range:U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;}
  @font-face{font-family:'Roboto';font-style:normal;font-weight:500;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v29/KFOlCnqEu92Fr1MmEU9fCRc4EsA.woff2) format('woff2');unicode-range:U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;}
  @font-face{font-family:'Roboto';font-style:normal;font-weight:500;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v29/KFOlCnqEu92Fr1MmEU9fABc4EsA.woff2) format('woff2');unicode-range:U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;}
  @font-face{font-family:'Roboto';font-style:normal;font-weight:500;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v29/KFOlCnqEu92Fr1MmEU9fCBc4EsA.woff2) format('woff2');unicode-range:U+1F00-1FFF;}
  @font-face{font-family:'Roboto';font-style:normal;font-weight:500;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v29/KFOlCnqEu92Fr1MmEU9fBxc4EsA.woff2) format('woff2');unicode-range:U+0370-03FF;}
  @font-face{font-family:'Roboto';font-style:normal;font-weight:500;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v29/KFOlCnqEu92Fr1MmEU9fCxc4EsA.woff2) format('woff2');unicode-range:U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;}
  @font-face{font-family:'Roboto';font-style:normal;font-weight:500;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v29/KFOlCnqEu92Fr1MmEU9fChc4EsA.woff2) format('woff2');unicode-range:U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;}
  @font-face{font-family:'Roboto';font-style:normal;font-weight:500;font-display:swap;src:url(https://fonts.gstatic.com/s/roboto/v29/KFOlCnqEu92Fr1MmEU9fBBc4.woff2) format('woff2');unicode-range:U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;}
  @font-face{font-family:'Open Sans';font-style:normal;font-weight:300;font-stretch:100%;src:url(https://fonts.gstatic.com/s/opensans/v27/memvYaGs126MiZpBA-UvWbX2vVnXBbObj2OVTSKmu1aB.woff2) format('woff2');unicode-range:U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;}
  @font-face{font-family:'Open Sans';font-style:normal;font-weight:300;font-stretch:100%;src:url(https://fonts.gstatic.com/s/opensans/v27/memvYaGs126MiZpBA-UvWbX2vVnXBbObj2OVTSumu1aB.woff2) format('woff2');unicode-range:U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;}
  @font-face{font-family:'Open Sans';font-style:normal;font-weight:300;font-stretch:100%;src:url(https://fonts.gstatic.com/s/opensans/v27/memvYaGs126MiZpBA-UvWbX2vVnXBbObj2OVTSOmu1aB.woff2) format('woff2');unicode-range:U+1F00-1FFF;}
  @font-face{font-family:'Open Sans';font-style:normal;font-weight:300;font-stretch:100%;src:url(https://fonts.gstatic.com/s/opensans/v27/memvYaGs126MiZpBA-UvWbX2vVnXBbObj2OVTSymu1aB.woff2) format('woff2');unicode-range:U+0370-03FF;}
  @font-face{font-family:'Open Sans';font-style:normal;font-weight:300;font-stretch:100%;src:url(https://fonts.gstatic.com/s/opensans/v27/memvYaGs126MiZpBA-UvWbX2vVnXBbObj2OVTS2mu1aB.woff2) format('woff2');unicode-range:U+0590-05FF, U+20AA, U+25CC, U+FB1D-FB4F;}
  @font-face{font-family:'Open Sans';font-style:normal;font-weight:300;font-stretch:100%;src:url(https://fonts.gstatic.com/s/opensans/v27/memvYaGs126MiZpBA-UvWbX2vVnXBbObj2OVTSCmu1aB.woff2) format('woff2');unicode-range:U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;}
  @font-face{font-family:'Open Sans';font-style:normal;font-weight:300;font-stretch:100%;src:url(https://fonts.gstatic.com/s/opensans/v27/memvYaGs126MiZpBA-UvWbX2vVnXBbObj2OVTSGmu1aB.woff2) format('woff2');unicode-range:U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;}
  @font-face{font-family:'Open Sans';font-style:normal;font-weight:300;font-stretch:100%;src:url(https://fonts.gstatic.com/s/opensans/v27/memvYaGs126MiZpBA-UvWbX2vVnXBbObj2OVTS-muw.woff2) format('woff2');unicode-range:U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;}
  @font-face{font-family:'Open Sans';font-style:normal;font-weight:400;font-stretch:100%;src:url(https://fonts.gstatic.com/s/opensans/v27/memvYaGs126MiZpBA-UvWbX2vVnXBbObj2OVTSKmu1aB.woff2) format('woff2');unicode-range:U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;}
  @font-face{font-family:'Open Sans';font-style:normal;font-weight:400;font-stretch:100%;src:url(https://fonts.gstatic.com/s/opensans/v27/memvYaGs126MiZpBA-UvWbX2vVnXBbObj2OVTSumu1aB.woff2) format('woff2');unicode-range:U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;}
  @font-face{font-family:'Open Sans';font-style:normal;font-weight:400;font-stretch:100%;src:url(https://fonts.gstatic.com/s/opensans/v27/memvYaGs126MiZpBA-UvWbX2vVnXBbObj2OVTSOmu1aB.woff2) format('woff2');unicode-range:U+1F00-1FFF;}
  @font-face{font-family:'Open Sans';font-style:normal;font-weight:400;font-stretch:100%;src:url(https://fonts.gstatic.com/s/opensans/v27/memvYaGs126MiZpBA-UvWbX2vVnXBbObj2OVTSymu1aB.woff2) format('woff2');unicode-range:U+0370-03FF;}
  @font-face{font-family:'Open Sans';font-style:normal;font-weight:400;font-stretch:100%;src:url(https://fonts.gstatic.com/s/opensans/v27/memvYaGs126MiZpBA-UvWbX2vVnXBbObj2OVTS2mu1aB.woff2) format('woff2');unicode-range:U+0590-05FF, U+20AA, U+25CC, U+FB1D-FB4F;}
  @font-face{font-family:'Open Sans';font-style:normal;font-weight:400;font-stretch:100%;src:url(https://fonts.gstatic.com/s/opensans/v27/memvYaGs126MiZpBA-UvWbX2vVnXBbObj2OVTSCmu1aB.woff2) format('woff2');unicode-range:U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;}
  @font-face{font-family:'Open Sans';font-style:normal;font-weight:400;font-stretch:100%;src:url(https://fonts.gstatic.com/s/opensans/v27/memvYaGs126MiZpBA-UvWbX2vVnXBbObj2OVTSGmu1aB.woff2) format('woff2');unicode-range:U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;}
  @font-face{font-family:'Open Sans';font-style:normal;font-weight:400;font-stretch:100%;src:url(https://fonts.gstatic.com/s/opensans/v27/memvYaGs126MiZpBA-UvWbX2vVnXBbObj2OVTS-muw.woff2) format('woff2');unicode-range:U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;}
  @font-face{font-family:'Open Sans';font-style:normal;font-weight:600;font-stretch:100%;src:url(https://fonts.gstatic.com/s/opensans/v27/memvYaGs126MiZpBA-UvWbX2vVnXBbObj2OVTSKmu1aB.woff2) format('woff2');unicode-range:U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;}
  @font-face{font-family:'Open Sans';font-style:normal;font-weight:600;font-stretch:100%;src:url(https://fonts.gstatic.com/s/opensans/v27/memvYaGs126MiZpBA-UvWbX2vVnXBbObj2OVTSumu1aB.woff2) format('woff2');unicode-range:U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;}
  @font-face{font-family:'Open Sans';font-style:normal;font-weight:600;font-stretch:100%;src:url(https://fonts.gstatic.com/s/opensans/v27/memvYaGs126MiZpBA-UvWbX2vVnXBbObj2OVTSOmu1aB.woff2) format('woff2');unicode-range:U+1F00-1FFF;}
  @font-face{font-family:'Open Sans';font-style:normal;font-weight:600;font-stretch:100%;src:url(https://fonts.gstatic.com/s/opensans/v27/memvYaGs126MiZpBA-UvWbX2vVnXBbObj2OVTSymu1aB.woff2) format('woff2');unicode-range:U+0370-03FF;}
  @font-face{font-family:'Open Sans';font-style:normal;font-weight:600;font-stretch:100%;src:url(https://fonts.gstatic.com/s/opensans/v27/memvYaGs126MiZpBA-UvWbX2vVnXBbObj2OVTS2mu1aB.woff2) format('woff2');unicode-range:U+0590-05FF, U+20AA, U+25CC, U+FB1D-FB4F;}
  @font-face{font-family:'Open Sans';font-style:normal;font-weight:600;font-stretch:100%;src:url(https://fonts.gstatic.com/s/opensans/v27/memvYaGs126MiZpBA-UvWbX2vVnXBbObj2OVTSCmu1aB.woff2) format('woff2');unicode-range:U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;}
  @font-face{font-family:'Open Sans';font-style:normal;font-weight:600;font-stretch:100%;src:url(https://fonts.gstatic.com/s/opensans/v27/memvYaGs126MiZpBA-UvWbX2vVnXBbObj2OVTSGmu1aB.woff2) format('woff2');unicode-range:U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;}
  @font-face{font-family:'Open Sans';font-style:normal;font-weight:600;font-stretch:100%;src:url(https://fonts.gstatic.com/s/opensans/v27/memvYaGs126MiZpBA-UvWbX2vVnXBbObj2OVTS-muw.woff2) format('woff2');unicode-range:U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;}
  @font-face{font-family:'Open Sans';font-style:normal;font-weight:700;font-stretch:100%;src:url(https://fonts.gstatic.com/s/opensans/v27/memvYaGs126MiZpBA-UvWbX2vVnXBbObj2OVTSKmu1aB.woff2) format('woff2');unicode-range:U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;}
  @font-face{font-family:'Open Sans';font-style:normal;font-weight:700;font-stretch:100%;src:url(https://fonts.gstatic.com/s/opensans/v27/memvYaGs126MiZpBA-UvWbX2vVnXBbObj2OVTSumu1aB.woff2) format('woff2');unicode-range:U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;}
  @font-face{font-family:'Open Sans';font-style:normal;font-weight:700;font-stretch:100%;src:url(https://fonts.gstatic.com/s/opensans/v27/memvYaGs126MiZpBA-UvWbX2vVnXBbObj2OVTSOmu1aB.woff2) format('woff2');unicode-range:U+1F00-1FFF;}
  @font-face{font-family:'Open Sans';font-style:normal;font-weight:700;font-stretch:100%;src:url(https://fonts.gstatic.com/s/opensans/v27/memvYaGs126MiZpBA-UvWbX2vVnXBbObj2OVTSymu1aB.woff2) format('woff2');unicode-range:U+0370-03FF;}
  @font-face{font-family:'Open Sans';font-style:normal;font-weight:700;font-stretch:100%;src:url(https://fonts.gstatic.com/s/opensans/v27/memvYaGs126MiZpBA-UvWbX2vVnXBbObj2OVTS2mu1aB.woff2) format('woff2');unicode-range:U+0590-05FF, U+20AA, U+25CC, U+FB1D-FB4F;}
  @font-face{font-family:'Open Sans';font-style:normal;font-weight:700;font-stretch:100%;src:url(https://fonts.gstatic.com/s/opensans/v27/memvYaGs126MiZpBA-UvWbX2vVnXBbObj2OVTSCmu1aB.woff2) format('woff2');unicode-range:U+0102-0103, U+0110-0111, U+0128-0129, U+0168-0169, U+01A0-01A1, U+01AF-01B0, U+1EA0-1EF9, U+20AB;}
  @font-face{font-family:'Open Sans';font-style:normal;font-weight:700;font-stretch:100%;src:url(https://fonts.gstatic.com/s/opensans/v27/memvYaGs126MiZpBA-UvWbX2vVnXBbObj2OVTSGmu1aB.woff2) format('woff2');unicode-range:U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;}
  @font-face{font-family:'Open Sans';font-style:normal;font-weight:700;font-stretch:100%;src:url(https://fonts.gstatic.com/s/opensans/v27/memvYaGs126MiZpBA-UvWbX2vVnXBbObj2OVTS-muw.woff2) format('woff2');unicode-range:U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;}
  @font-face{font-family:'FontAwesome';src:url('https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/fonts/fontawesome-webfont.eot?v=4.7.0');src:url('https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/fonts/fontawesome-webfont.eot#iefix&v=4.7.0') format('embedded-opentype'),url('https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/fonts/fontawesome-webfont.woff2?v=4.7.0') format('woff2'),url('https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/fonts/fontawesome-webfont.woff?v=4.7.0') format('woff'),url('https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/fonts/fontawesome-webfont.ttf?v=4.7.0') format('truetype'),url('https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/fonts/fontawesome-webfont.svg?v=4.7.0#fontawesomeregular') format('svg');font-weight:normal;font-style:normal;}`;

  css = `   /*! CSS Used from: https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css */
  .fa {
      display: inline-block;
      font: normal normal normal 14px/1 FontAwesome;
      font-size: inherit;
      text-rendering: auto;
      -webkit-font-smoothing: antialiased;
      -moz-osx-font-smoothing: grayscale;
  }

  .fa-circle:before {
      content: "\f111";
  }

  /*! CSS Used from: https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css */
  *,
  ::after,
  ::before {
      box-sizing: border-box;
  }

  hr {
      margin: 1rem 0;
      color: inherit;
      background-color: currentColor;
      border: 0;
      opacity: .25;
  }

  hr:not([size]) {
      height: 1px;
  }

  h1 {
      margin-top: 0;
      margin-bottom: .5rem;
      font-weight: 500;
      line-height: 1.2;
  }

  h1 {
      font-size: calc(1.375rem + 1.5vw);
  }

  @media (min-width:1200px) {
      h1 {
          font-size: 2.5rem;
      }
  }

  b {
      font-weight: bolder;
  }

  a {
      color: #0d6efd;
      text-decoration: underline;
  }

  a:hover {
      color: #0a58ca;
  }

  button {
      border-radius: 0;
  }

  button:focus:not(:focus-visible) {
      outline: 0;
  }

  button {
      margin: 0;
      font-family: inherit;
      font-size: inherit;
      line-height: inherit;
  }

  button {
      text-transform: none;
  }

  button {
      -webkit-appearance: button;
  }

  ::-moz-focus-inner {
      padding: 0;
      border-style: none;
  }

  .container {
      width: 100%;
      padding-right: var(--bs-gutter-x, .75rem);
      padding-left: var(--bs-gutter-x, .75rem);
      margin-right: auto;
      margin-left: auto;
  }

  @media (min-width:576px) {
      .container {
          max-width: 540px;
      }
  }

  @media (min-width:768px) {
      .container {
          max-width: 720px;
      }
  }

  @media (min-width:992px) {
      .container {
          max-width: 960px;
      }
  }

  @media (min-width:1200px) {
      .container {
          max-width: 1140px;
      }
  }

  @media (min-width:1400px) {
      .container {
          max-width: 1320px;
      }
  }

  .row {
      --bs-gutter-x: 1.5rem;
      --bs-gutter-y: 0;
      display: flex;
      flex-wrap: wrap;
      margin-top: calc(-1 * var(--bs-gutter-y));
      margin-right: calc(-.5 * var(--bs-gutter-x));
      margin-left: calc(-.5 * var(--bs-gutter-x));
  }

  .row>* {
      flex-shrink: 0;
      width: 100%;
      max-width: 100%;
      padding-right: calc(var(--bs-gutter-x) * .5);
      padding-left: calc(var(--bs-gutter-x) * .5);
      margin-top: var(--bs-gutter-y);
  }

  .col-1 {
      flex: 0 0 auto;
      width: 8.33333333%;
  }

  .col-2 {
      flex: 0 0 auto;
      width: 16.66666667%;
  }

  .col-4 {
      flex: 0 0 auto;
      width: 33.33333333%;
  }

  .col-5 {
      flex: 0 0 auto;
      width: 41.66666667%;
  }

  .col-7 {
      flex: 0 0 auto;
      width: 58.33333333%;
  }

  .col-9 {
      flex: 0 0 auto;
      width: 75%;
  }

  .col-12 {
      flex: 0 0 auto;
      width: 100%;
  }

  @media (min-width:576px) {
      .col-sm-2 {
          flex: 0 0 auto;
          width: 16.66666667%;
      }

      .col-sm-5 {
          flex: 0 0 auto;
          width: 41.66666667%;
      }

      .col-sm-6 {
          flex: 0 0 auto;
          width: 50%;
      }

      .col-sm-7 {
          flex: 0 0 auto;
          width: 58.33333333%;
      }
  }

  @media (min-width:768px) {
      .col-md-12 {
          flex: 0 0 auto;
          width: 100%;
      }
  }

  .btn {
      display: inline-block;
      font-weight: 400;
      line-height: 1.5;
      color: #212529;
      text-align: center;
      text-decoration: none;
      vertical-align: middle;
      cursor: pointer;
      -webkit-user-select: none;
      -moz-user-select: none;
      user-select: none;
      background-color: transparent;
      border: 1px solid transparent;
      padding: .375rem .75rem;
      font-size: 1rem;
      border-radius: .25rem;
  }

  @media (prefers-reduced-motion:reduce) {
      .btn {
          transition: none;
      }
  }

  .btn:hover {
      color: #212529;
  }

  .btn:focus {
      outline: 0;
  }

  .btn:disabled {
      pointer-events: none;
      opacity: .65;
  }

  .btn-info {
      color: #000;
      background-color: #0dcaf0;
      border-color: #0dcaf0;
  }

  .btn-info:hover {
      color: #000;
      background-color: #31d2f2;
      border-color: #25cff2;
  }

  .btn-info:focus {
      color: #000;
      background-color: #31d2f2;
      border-color: #25cff2;
  }

  .btn-info:active {
      color: #000;
      background-color: #3dd5f3;
      border-color: #25cff2;
  }

  .btn-info:active:focus {
  }

  .btn-info:disabled {
      color: #000;
      background-color: #0dcaf0;
      border-color: #0dcaf0;
  }

  .align-middle {
      vertical-align: middle !important;
  }

  .d-none {
      display: none !important;
  }

  .justify-content-end {
      justify-content: flex-end !important;
  }

  .align-items-center {
      align-items: center !important;
  }

  .align-self-start {
      align-self: flex-start !important;
  }

  .order-first {
      order: -1 !important;
  }

  .mx-2 {
      margin-right: .5rem !important;
      margin-left: .5rem !important;
  }

  .my-1 {
      margin-top: .25rem !important;
      margin-bottom: .25rem !important;
  }

  .my-2 {
      margin-top: .5rem !important;
      margin-bottom: .5rem !important;
  }

  .mt-1 {
      margin-top: .25rem !important;
  }

  .mt-2 {
      margin-top: .5rem !important;
  }

  .mt-3 {
      margin-top: 1rem !important;
  }

  .mt-4 {
      margin-top: 1.5rem !important;
  }

  .mb-2 {
      margin-bottom: .5rem !important;
  }

  .mb-4 {
      margin-bottom: 1.5rem !important;
  }

  .p-2 {
      padding: .5rem !important;
  }

  .px-0 {
      padding-right: 0 !important;
      padding-left: 0 !important;
  }

  .px-4 {
      padding-right: 1.5rem !important;
      padding-left: 1.5rem !important;
  }

  .text-center {
      text-align: center !important;
  }

  .text-capitalize {
      text-transform: capitalize !important;
  }

  .text-white {
      --bs-text-opacity: 1;
      color: rgba(var(--bs-white-rgb), var(--bs-text-opacity)) !important;
  }

  @media (min-width:576px) {
      .d-sm-block {
          display: block !important;
      }

      .d-sm-flex {
          display: flex !important;
      }

      .d-sm-none {
          display: none !important;
      }

      .order-sm-last {
          order: 6 !important;
      }

      .mb-sm-0 {
          margin-bottom: 0 !important;
      }
  }

  @media (min-width:992px) {
      .mt-lg-0 {
          margin-top: 0 !important;
      }
  }

  /*! CSS Used from: Embedded */
  .text-secondary-d1 {
      color: #728299 !important;
  }

  .brc-default-l1 {
      border-color: #dce9f0 !important;
  }

  .mx-n1 {
      margin-left: -.25rem !important;
  }

  .mx-n1 {
      margin-right: -.25rem !important;
  }

  .mb-4 {
      margin-bottom: 1.5rem !important;
  }

  hr {
      margin-top: 1rem;
      margin-bottom: 1rem;
      border: 0;
      border-top: 1px solid rgba(0, 0, 0, .1);
  }

  .text-grey-m2 {
      color: #888a8d !important;
  }

  .text-600 {
      font-weight: 600 !important;
  }

  .text-110 {
      font-size: 110% !important;
  }

  .text-blue {
      color: #478fcc !important;
  }

  .py-25 {
      padding-bottom: .75rem !important;
  }

  .py-25 {
      padding-top: .75rem !important;
  }

  .bgc-default-tp1 {
      background-color: rgba(121, 169, 197, .92) !important;
  }

  .text-120 {
      font-size: 120% !important;
  }

  .text-blue-m2 {
      color: #68a3d5 !important;
  }

  .text-150 {
      font-size: 150% !important;
  }

  .page {
      background: white;
      display: block;
      margin: 0 auto;
  }

  .page[size="A4"] {
      width: 21cm;
      height: 29.7cm;
  }

  @media print {
      .page {
          margin: 0;
          box-shadow: 0;
      }
  }

  /*! CSS Used fontfaces */
  @font-face {
      font-family: 'FontAwesome';
      src: url('https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/fonts/fontawesome-webfont.eot?v=4.7.0');
      src: url('https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/fonts/fontawesome-webfont.eot#iefix&v=4.7.0') format('embedded-opentype'), url('https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/fonts/fontawesome-webfont.woff2?v=4.7.0') format('woff2'), url('https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/fonts/fontawesome-webfont.woff?v=4.7.0') format('woff'), url('https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/fonts/fontawesome-webfont.ttf?v=4.7.0') format('truetype'), url('https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/fonts/fontawesome-webfont.svg?v=4.7.0#fontawesomeregular') format('svg');
      font-weight: normal;
      font-style: normal;
  }
  body {
    margin-top: 20px;
    color: #484b51;
}

.text-secondary-d1 {
    color: #728299 !important;
}

.page-header {
    /* margin: 0 0 1rem; */
    padding-bottom: 1rem;
    padding-top: .5rem;
    border-bottom: 1px dotted #e2e2e2;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-pack: justify;
    justify-content: space-between;
    -ms-flex-align: center;
    align-items: center;
}

.page-title {
    padding: 0;
    margin: 0;
    font-size: 1.75rem;
    font-weight: 300;
}

.brc-default-l1 {
    border-color: #dce9f0 !important;
}

.ml-n1, .mx-n1 {
    margin-left: -.25rem !important;
}

.mr-n1, .mx-n1 {
    margin-right: -.25rem !important;
}

.mb-4, .my-4 {
    margin-bottom: 1.5rem !important;
}

hr {
    margin-top: 1rem;
    margin-bottom: 1rem;
    border: 0;
    border-top: 1px solid rgba(0, 0, 0, .1);
}

.text-grey-m2 {
    color: #888a8d !important;
}

.text-success-m2 {
    color: #86bd68 !important;
}

.font-bolder, .text-600 {
    font-weight: 600 !important;
}

.text-110 {
    font-size: 110% !important;
}

.text-blue {
    color: #478fcc !important;
}

.pb-25, .py-25 {
    padding-bottom: .75rem !important;
}

.pt-25, .py-25 {
    padding-top: .75rem !important;
}

.bgc-default-tp1 {
    background-color: rgba(121, 169, 197, .92) !important;
}

.bgc-default-l4, .bgc-h-default-l4:hover {
    background-color: #f3f8fa !important;
}

.page-header
.page-tools {
    -ms-flex-item-align: end;
    align-self: flex-end;
}

.btn-light {
    color: #757984;
    background-color: #f5f6f9;
    border-color: #dddfe4;
}

.w-2 {
    width: 1rem;
}

.text-120 {
    font-size: 120% !important;
}

.text-primary-m1 {
    color: #4087d4 !important;
}

.text-danger-m1 {
    color: #dd4949 !important;
}

.text-blue-m2 {
    color: #68a3d5 !important;
}

.text-150 {
    font-size: 150% !important;
}

.text-60 {
    font-size: 60% !important;
}

.text-grey-m1 {
    color: #7b7d81 !important;
}

.align-bottom {
    vertical-align: bottom !important;
}

body {
  background: white;
}

.page {
    background: white;
    display: block;
    margin: 0 auto;

}

.page[size="A4"] {
    width: 21cm;
    height: 29.7cm;
}

.page[size="A4"][layout="landscape"] {
    width: 29.7cm;
    height: 21cm;
}

.page[size="A3"] {
    width: 29.7cm;
    height: 42cm;
}

.page[size="A3"][layout="landscape"] {
    width: 42cm;
    height: 29.7cm;
}

.page[size="A5"] {
    width: 14.8cm;
    height: 21cm;
}

.page[size="A5"][layout="landscape"] {
    width: 21cm;
    height: 14.8cm;
}

@media print {
    body, .page {
        margin: 0;
        box-shadow: 0;
    }
}

@media print {

}
  `;




  @ViewChild('invoice')
  invoice!: ElementRef;

  getRouteParam() {
    this.route.params.subscribe((params) => {

        this.routeParams = true;
        this.routeParams = params.invoiceId;
        this.getInvoices(params.invoiceId);

    });
  }

  getInvoices(invoiceId:number){
    this.ledgerService.getInvoice(invoiceId).subscribe( res => {
      this.invoiceData = res;
    });
  }

  getAllStock(){
    this.stockService.getAllStock().subscribe( res => {
      this.stockData = res;

      this.stockData.forEach((stock:any) => {
          this.stockDataObject[stock.id] = {stock_name: stock.stock_name}
      });

    })
  }

  getStockName(id:number){
    return this.stockDataObject[id].stock_name;
  }


  printSelectedArea(){

    // let innerContents = this.invoice_print.nativeElement.innerHTML;
    let innerContents = document.getElementById('invoice_print')?.innerHTML;

    let popupWinindow:any = window.open('', '_blank', 'scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');

    popupWinindow.document.open();

    popupWinindow.document.write('<html><head><style>'+this.css + this.css2+'</style> </head><body  onload="window.print()">' + innerContents + '</html>');
    popupWinindow.document.close();

  }

}

