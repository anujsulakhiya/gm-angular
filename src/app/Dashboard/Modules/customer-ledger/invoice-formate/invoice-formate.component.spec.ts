import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoiceFormateComponent } from './invoice-formate.component';

describe('InvoiceFormateComponent', () => {
  let component: InvoiceFormateComponent;
  let fixture: ComponentFixture<InvoiceFormateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InvoiceFormateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoiceFormateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
