import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CustomerLedgerRoutingModule } from './customer-ledger-routing.module';
import { ViewLedgerComponent } from './view-ledger/view-ledger.component';
import {AutocompleteLibModule} from "angular-ng-autocomplete";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { PaymentTypePipe } from './payment-type.pipe';
import {NgbCollapseModule} from "@ng-bootstrap/ng-bootstrap";
import { PaymentModePipe } from './payment-mode.pipe';

@NgModule({
  declarations: [
    ViewLedgerComponent,
    PaymentTypePipe,
    PaymentModePipe
  ],
  imports: [
    CommonModule,
    CustomerLedgerRoutingModule,
    ReactiveFormsModule,
    AutocompleteLibModule,
    NgbCollapseModule,
    FormsModule,
  ]
})
export class CustomerLedgerModule { }
