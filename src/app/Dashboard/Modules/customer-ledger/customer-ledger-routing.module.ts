import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {CustomerLedgerComponent} from "./customer-ledger.component";
import { InvoiceFormateComponent } from './invoice-formate/invoice-formate.component';
import {ViewLedgerComponent} from "./view-ledger/view-ledger.component";

const routes: Routes = [
  {
    path: '',
    component: CustomerLedgerComponent,
    children: [
      {
        path: '',
        redirectTo: 'ViewLedger',
        pathMatch: 'full',
      },
      {
        path: 'ViewLedger',
        component: ViewLedgerComponent,
      },
      // {
      //   path: 'PrintInvoice/:invoiceId', component: InvoiceFormateComponent
      // },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomerLedgerRoutingModule { }
