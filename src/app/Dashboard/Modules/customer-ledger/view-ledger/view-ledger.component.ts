import {Component, OnInit, ViewChild} from '@angular/core';
import {
  BankInterface,
  BookingInterface,
  CustomerInterface,
  GetLedgerInterface,
  invoiceCustomerId,
  InvoiceInterface, InvoiceNumberInterface
} from "../../../dashboard.interfaces";
import {BookingService} from "../../booking/booking.service";
import {InvoiceService} from "../../booking/invoice.service";
import {CustomerMasterService} from "../../../MasterModules/customer-master/customer-master.service";
import {FormArray, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {CustomerLedgerService} from "../customer-ledger.service";
import {DatePipe} from "@angular/common";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {StockReturnService} from "../../stock-return/stock-return.service";
import {paymentType, PaymentTypePipe} from "../payment-type.pipe";
import {paymentMode, PaymentModePipe} from "../payment-mode.pipe";
import {BankMasterService} from "../../../MasterModules/bank-master/bank-master.service";
import {AlertService} from "../../../../services/alert.service";
import {any} from "codelyzer/util/function";

@Component({
  selector: 'app-view-ledger',
  templateUrl: './view-ledger.component.html',
  styleUrls: ['./view-ledger.component.css']
})
export class ViewLedgerComponent {

  today:any = new Date();
  //FormGroup Object
  createCustomerLedger: FormGroup | any;
  editCustomerLedger: FormGroup | any;

  bookingData: BookingInterface[] = [];

  customerDetails: CustomerInterface[] = [];
  bankDetails: BankInterface[] = [];
  currentCustomer: CustomerInterface[] |any= [];
  customerLedger: any[] = [];
  openingBalance: any = 0;
  singleLedger: any = [];
  stockReturnStatus: any;
  customerId: invoiceCustomerId = {
    invoice_customer_id: 0
  };
  invoiceDetails: InvoiceInterface[] |any= [];
  invoiceNo: InvoiceNumberInterface = {
    invoice_number: ''
  };
  paymentType: string[]|any = [];
  paymentMode: string[] |any= [];
  //Status Variable
  invoiceStatus = false;
  public invoiceCollapsed = true;
  public ledgerCollapsed = false;
  noDataFound = false;
  noLedgerFound = false;
  noInvoiceFound = false;
  noStockReturnFound = false;
  noCustomerSelected  = true;
  paymentModeBank = false;
  paymentModeCheque = false;
  ledgerInfo: GetLedgerInterface|any = [];
  receiptNo:any;

  constructor( private datePipe: DatePipe,
                private customerService: CustomerMasterService,
                private bankService: BankMasterService,
                private formBuilder: FormBuilder,
                private customerLedgerService: CustomerLedgerService,
               private modalService: NgbModal,
               private sweetAlert: AlertService,
               private stockReturnService: StockReturnService) {

    this.paymentType = paymentType;
    this.paymentMode = paymentMode;
    this.setDate();
    this.generateReceiptNo();
    this.getAllCustomers();
    this.initialization();
    this.EditLedgerInitialization(this.singleLedger);
    this.getAllBank();
    // this.forTesting();
  }

  // ------------ Set Current Date ------------ //
  setDate() {
    this.today = this.datePipe.transform(this.today, 'yyyy-MM-dd');
  }

  generateReceiptNo(){
      this.customerLedgerService.getLatestReceiptNo().subscribe((res) => {

      this.receiptNo = 0;
      if (res) {

       this.receiptNo = res;

       this.getCustomerInvoice(this.currentCustomer.id);
      }
    });
  }

  generateFields(customerInvoice: InvoiceInterface): FormGroup {
    return this.formBuilder.group({

      tdate: [this.today, [Validators.required]],
      voucher_id: [this.receiptNo, [Validators.nullValidator]],
      account_id: [1, [Validators.required]],

      invoice_date: [customerInvoice.invoice_date, [Validators.required]],
      invoice_id: [customerInvoice.id, [Validators.required]],
      invoice_number: [customerInvoice.invoice_number, [Validators.required]],
      old_balance: [customerInvoice.paid_amount + customerInvoice.discount, [Validators.required]],
      due_amount: [(customerInvoice.invoice_total + customerInvoice.debit) - (customerInvoice.paid_amount + customerInvoice.discount), [Validators.required]],
      bill_amount: [customerInvoice.invoice_total + customerInvoice.debit, [Validators.required]],
      total_remaining_qty: [customerInvoice.total_remaining_qty, [Validators.required]],
      customer_id: [customerInvoice.invoice_customer_id, [Validators.required]],

      debit: [0, [Validators.required]],
      payment_in: [0, [Validators.required]],
      payment_out: [0, [Validators.required]],
      discount: [0, [Validators.required]],

      balance: ['', [Validators.required]],
      payment_id: ['', [Validators.required]],
      bank_id: ['', [Validators.nullValidator]],
      cheque_no: ['', [Validators.nullValidator]],
      cheque_clear_date: ['', [Validators.nullValidator]],
      cheque_date: ['', [Validators.nullValidator]],
      remark: ['', [Validators.nullValidator]],
    });
  }

  // Initialize Customer Ledger Entry Form
  initialization() {
    this.createCustomerLedger = this.formBuilder.group({
      invoice: new FormArray([]),
    });
  }

  // Initialize Customer Ledger Entry Form
  EditLedgerInitialization(ledgerDetail: any) {
    this.editCustomerLedger = this.formBuilder.group({
      tdate: [ledgerDetail.tdate, [Validators.required]],
      voucher_id: [ledgerDetail.voucher_id, [Validators.required]],

      invoice_id: [ledgerDetail.invoice_id, [Validators.required]],
      customer_id: [ledgerDetail.customer_id, [Validators.required]],

      debit: [ledgerDetail.debit, [Validators.required]],
      payment_in: [ledgerDetail.payment_in, [Validators.required]],
      payment_out: [ledgerDetail.payment_out, [Validators.required]],
      discount: [ledgerDetail.discount, [Validators.required]],

      balance: ['', [Validators.required]],
      payment_id: [ledgerDetail.payment_id, [Validators.required]],
      bank_id: [ledgerDetail.bank_id, [Validators.nullValidator]],
      cheque_no: [ledgerDetail.cheque_no, [Validators.nullValidator]],
      cheque_clear_date: [ledgerDetail.cheque_clear_date, [Validators.nullValidator]],
      cheque_date: [ledgerDetail.cheque_date, [Validators.nullValidator]],
      remark: [ledgerDetail.remark, [Validators.nullValidator]],
    });
  }

  get invoice() : FormArray {
    return this.createCustomerLedger.get("invoice") as FormArray
  }

  //Getting all Customer
  getAllCustomers() {

    this.customerService.getAllCustomer().subscribe((res) => {

      if(res.length > 0){
        this.customerDetails = [];
        this.customerDetails = res;

      } else {
        this.sweetAlert.alert('No Customer Found','Please Create Customer From Customer Master Section','warning')
      }



    });
  }

  //Getting all Customer
  getAllBank() {
    this.bankService.getAllBank().subscribe((res) => {
      this.bankDetails = [];
      this.bankDetails = res;
    });
  }

  //Get Customer Ledger
  getCustomerInvoice(customerId: any){
    this.customerLedgerService.getCustomerInvoice(customerId).subscribe( res => {

      if(res.length > 0){
        this.invoiceDetails = res;

        this.invoice.controls = [];
        this.invoiceDetails.forEach( (invoice: InvoiceInterface) => {
          this.invoice.push(this.generateFields(invoice));
        });

        this.noInvoiceFound = false;
      } else {
        this.noInvoiceFound = true;
      }
      this.noCustomerSelected = false;

    });
  }

  //Get Customer Ledger
  getCustomerLedger(id:number){
    //
    // let ledgerId: GetLedgerInterface = {
    //   customer_id: id
    // }
    this.customerLedgerService.getCustomerLedger(id).subscribe( res => {



      this.openingBalance = 0;
      this.openingBalance = res.opening_balance;


      let i=0;
      let lastTotal = 0;
      res.report.forEach( (ledger:any) => {

        if(i == 0){
          ledger.running_total = this.openingBalance + ledger.balance;
          lastTotal = ledger.running_total;
        } else {
          ledger.running_total = lastTotal + ledger.balance;
          lastTotal = ledger.running_total;
        }
        i++;
      })


      this.noLedgerFound = res.report.length <= 0;
      this.customerLedger = res.report;






      // this.getCustomerInvoice(id);
      // this.noInvoiceFound = res.ActiveInvoice.length <= 0;
      // this.invoiceDetails = res.ActiveInvoice;
      //
      // this.invoiceDetails.forEach( (invoice: InvoiceInterface) => {
      //   this.invoice.push(this.generateFields(invoice));
      // });

      this.noCustomerSelected = false;
    });
  }


  //Get Customer Ledger
  getCustomerLedgerByDate(){

    this.customerLedgerService.getCustomerLedgerWithDate(this.currentCustomer.id,this.ledgerInfo).subscribe( res => {

      this.openingBalance = 0;
      this.openingBalance = res.opening_balance;


      let i=0;
      let lastTotal = 0;
      res.report.forEach( (ledger:any) => {

        if(i == 0){
          ledger.running_total = this.openingBalance + ledger.balance;
          lastTotal = ledger.running_total;
        } else {
          ledger.running_total = lastTotal + ledger.balance;
          lastTotal = ledger.running_total;
        }
        i++;
      })

      this.noLedgerFound = res.report.length <= 0;
      this.customerLedger = res.report;

      this.noCustomerSelected = false;
    });
  }

  viewAllLedger(){
    this.getCustomerLedger(this.currentCustomer.id);
  }

  calculateTotalAmount(index:any){
    let total  = this.invoice.controls[index].get('due_amount')?.value +  this.invoice.controls[index].get('debit')?.value
      - ( this.invoice.controls[index].get('payment_in')?.value + this.invoice.controls[index].get('discount')?.value)
      + this.invoice.controls[index].get('payment_out')?.value;

    this.invoice.controls[index].get('balance')?.setValue(total);
  }

  calculateEditedTotalAmount(){

    let total  = ( this.editCustomerLedger.get('payment_in')?.value + this.editCustomerLedger.get('discount')?.value) - (this.editCustomerLedger.get('debit')?.value
      + this.editCustomerLedger.get('payment_out')?.value);

    this.editCustomerLedger.get('balance')?.setValue(total);
  }

  createLedger(index:any){
    if(this.invoice.controls[index].valid){

      this.customerLedgerService.createLedger(this.invoice.controls[index].value).subscribe( res => {

        this.generateReceiptNo();
        this.customerLedger = res.ledger.report;

        // this.invoiceDetails = res.ActiveInvoice;
        //
        // this.invoice.controls = [];
        // this.invoiceDetails.forEach( (invoice: InvoiceInterface) => {
        //   this.invoice.push(this.generateFields(invoice));
        // });

        this.getCustomerLedger(this.currentCustomer.id);
      });
    }
  }

  editLedger(){

    if(this.editCustomerLedger.valid){

      this.customerLedgerService.editLedger(this.singleLedger.id,this.editCustomerLedger.value).subscribe( res => {
        this.generateReceiptNo();
        this.getCustomerLedger(res.customer_id);
        this.getCustomerInvoice(res.customer_id);
      });
    }
  }

  // ------------ Customer autocomplete ------------ //

  forTesting(){
    this.getCustomerInvoice(1);
    this.getCustomerLedger(1);
  }
  customerKeyword = 'customer_name';

  selectEventCustomer(item: any) {

    this.invoice.controls = [];
    this.customerLedger = [];
    this.getCustomerInvoice(item.id);
    this.getCustomerLedger(item.id);
    this.currentCustomer = item;
  }

  onCustomerCleared() {
    this.currentCustomer = [];
    this.noCustomerSelected = true;
  }

  customerFilter = function ( customerDetails: any[],query: string): CustomerInterface[] {
    return  customerDetails.filter((x) => x.customer_name.toLowerCase().startsWith(query));
  };

  editLedgerModel(stockReturnStatusModel: any, ledgerId: number) {
    this.getLedgerDetails(ledgerId);
    this.modalService.open(stockReturnStatusModel,{ size: 'xl', centered: true });
  }

  getLedgerDetails(ledgerId: number){
    this.customerLedgerService.getSingleCustomerLedger(ledgerId).subscribe( res => {
      this.singleLedger = res;

      this.EditLedgerInitialization(res);
    });
  }

  getStockReturnDetails(invoice_number: string){
    this.invoiceNo.invoice_number = invoice_number;
    this.invoiceNo.type = 2;

    this.stockReturnService.getStockReturnDetails(this.invoiceNo).subscribe( res => {

      this.stockReturnStatus = res;
      this.noStockReturnFound = res.length <= 0;
    });
  }

  checkPaymentType(event:any){
    this.paymentModeBank = event.target.value == 2;
    this.paymentModeCheque = event.target.value == 3;
  }

  deleteLedger(ledgerId: number) {
    this.customerLedgerService.deleteLedger(ledgerId).subscribe( res => {
      this.generateReceiptNo();
      this.getCustomerLedger(this.currentCustomer.id);
      this.getCustomerInvoice(this.currentCustomer.id);
    });
  }



}
