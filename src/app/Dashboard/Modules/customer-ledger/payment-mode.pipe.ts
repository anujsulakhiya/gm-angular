import { Pipe, PipeTransform } from '@angular/core';


const paymentMode: string[] = ['By Booking' ,'By Cash', 'By Bank','By Cheque', 'By Other'];


@Pipe({
  name: 'paymentMode'
})
class PaymentModePipe implements PipeTransform {

  transform(value: number): string {
    return paymentMode[value];
  }

}

export {PaymentModePipe, paymentMode}
