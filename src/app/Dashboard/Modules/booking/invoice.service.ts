import { Injectable } from '@angular/core';
import { HttpService } from '../../../services/http.service';
import { Observable } from 'rxjs';
import { InvoiceInterface } from '../../dashboard.interfaces';

@Injectable({
  providedIn: 'root',
})
export class InvoiceService {
  constructor(private httpService: HttpService) {}

  getAllInvoice(): Observable<InvoiceInterface[]> {
    return this.httpService.get('/invoice');
  }
}
