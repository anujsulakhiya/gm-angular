import { Component } from '@angular/core';
import { BookingService } from '../booking.service';
import {
  BookingInterface,
  InvoiceInterface,
} from '../../../dashboard.interfaces';
import { InvoiceService } from '../invoice.service';
import {AlertService} from "../../../../services/alert.service";

@Component({
  selector: 'app-booking-list',
  templateUrl: './booking-list.component.html',
  styleUrls: ['./booking-list.component.css'],
})
export class BookingListComponent {

  bookingData: BookingInterface[] = [];
  invoiceData: InvoiceInterface[] = [];
  constructor(
    private bookingService: BookingService,
    private invoiceService: InvoiceService,
    private sweetAlert: AlertService
  ) {
    this.getAllBookings();
  }

  getAllBookings() {
    this.invoiceService.getAllInvoice().subscribe((res) => {

      if(res.length > 0){
        this.invoiceData = res;
      } else  {
        this.sweetAlert.alert('No Invoice Found','Invoice List will Appear here after Booking is Created','warning');
      }


    });
  }

  deleteInvoice(invoice_id: number) {}
}
