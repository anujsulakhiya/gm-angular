import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BookingComponent } from './booking.component';
import { CreateBookingComponent } from './create-booking/create-booking.component';
import { EditBookingComponent } from './edit-booking/edit-booking.component';
import { BookingListComponent } from './booking-list/booking-list.component';

const routes: Routes = [
  {
    path: '',
    component: BookingComponent,
    children: [
      {
        path: '',
        redirectTo: 'CreateBooking',
        pathMatch: 'full',
      },
      {
        path: 'CreateBooking',
        component: CreateBookingComponent,
      },
      {
        path: 'CreateBooking/:invoice_number',
        component: CreateBookingComponent,
      },
      {
        path: 'EditBooking/:id',
        component: EditBookingComponent,
      },
      {
        path: 'BookingList',
        component: BookingListComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BookingRoutingModule {}
