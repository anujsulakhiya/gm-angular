import { Component, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BookingService } from '../booking.service';
import {
  BookingInterface,
  CustomerInterface,
  InvoiceNumberInterface,
  LatestInvoiceInterface,
  StockInterface,
} from '../../../dashboard.interfaces';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { StockMasterService } from '../../../MasterModules/stock-master/stock-master.service';
import { DatePipe } from '@angular/common';
import { CustomerMasterService } from '../../../MasterModules/customer-master/customer-master.service';
import {AlertService} from "../../../../services/alert.service";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-create-booking',
  templateUrl: './create-booking.component.html',
  styleUrls: ['./create-booking.component.css'],
  providers: [DatePipe],
})
export class CreateBookingComponent {
  //Today Date
  today = new Date();

  //FormGroup Object
  createBooking: FormGroup | any;
  editBooking: FormGroup | any;
  editBookingCustomer: FormGroup | any;

  //---Declaration Variables----//

  //Stock Details
  stockDetails: StockInterface[] = [];
  stockDetail: StockInterface[] = [
    {
      firm_id: 0,
      stock_name: '',
      stock_mrp: 0,
      stock_qty:0,
      stock_gst: 0,
      stock_type: false,
      is_inventory: 0,
      stock_value: 0,
    },
  ];
  stockAvailable:any = 0;

  //Customer Details
  customerDetails: CustomerInterface[] = [];
  customerDetail: CustomerInterface[] | any = [
    {
      id: 0,
      customer_name: '',
      customer_address: '',
      customer_contact_no: 0,
      customer_gstin: 0,
    },
  ];

  editCustomerDetail: CustomerInterface[] = [
    {
      id: 0,
      customer_name: '',
      customer_address: '',
      customer_contact_no: 0,
      customer_gstin: 0,
    },
  ];

  //Booking Detail
  bookingId = 0;
  service_id: any;
  serviceProvided : any[] = [];
  currentTotal = 0;
  bookingData: BookingInterface[] | any = [
    {
      id: 0,
      invoice_number: 1,
      invoice_id: 0,
      invoice_date: '',
      customer_name: '',
      customer_address: '',
      customer_contact_no: 0,
      customer_gstin: 0,
      stock_name: '',
      rate: 0,
      qty_out: 0,
      date: '',
      days: 0,
      time: '',
    },
  ];

  //Invoice
  invoiceNo: InvoiceNumberInterface | any= {
    invoice_number: '1',
  };
  newInvoiceNumber = 0;
  latestInvoiceData: LatestInvoiceInterface |any= {
    id: 0,
    invoice_number: 1,
  };
  InvoiceTotalAmount = 0;

  isInventoryValue : any;

  //Status Variables
  invoice = false;
  isInventory = false;
  editBookingStatus = false;
  BookingCreatedSuccess = false;
  editCustomerSuccess = false;
  editBookingSuccess = false;
  bookingDeleteSuccess = false;
  stockDetailStatus = true;
  customerDetailStatus = true;
  customerIsSelected = true;
  tabIndexStockInventory = false;
  tabIndexStock = false;
  routeParams = false;

  constructor(
    private datePipe: DatePipe,
    private bookingService: BookingService,
    private stockService: StockMasterService,
    private customerService: CustomerMasterService,
    private formBuilder: FormBuilder,
    private modalService: NgbModal,
    private sweetAlert: AlertService,
    private route: ActivatedRoute
  ) {
    this.setDate();
    this.initializeForm(this.bookingData, this.stockDetail);
    this.generateInvoiceNo();
    this.customerEdit(this.customerDetails);
    this.initializeFormEditBooking(this.bookingData);
    // this.forTesting();
    this.getRouteParam();
  }


  getRouteParam() {
    this.route.params.subscribe(params => {
      if(params.invoice_number){
        this.routeParams = true;
        this.invoiceNo.invoice_number = params.invoice_number;
        this.invoiceNo.type = 1;
        this.getBookingDetails(this.invoiceNo);
      } else {
        this.routeParams = false;
      }
    });
  }
  // ------------ Set Current Date ------------ //
  setDate() {
    this.bookingData[0].invoice_date = this.today
      ? this.datePipe.transform(this.today, 'yyyy-MM-dd')!
      : '';
  }

  // ------------ Generate New Invoice Number ------------ //
  generateInvoiceNo() {
    this.bookingService.getLatestInvoice().subscribe((res) => {

      this.serviceProvided = [];

      if (res) {
        this.latestInvoiceData = res;
        this.bookingData[0].invoice_number = +this.latestInvoiceData.invoice_number + 1;
      }

      this.getAllCustomer();
      this.getStockDetails();
      this.initializeForm(this.bookingData, this.stockDetail);
    });
  }

  // ------------ Functions to Get all details required for create booking ------------ //

  //Getting all bookings when an invoice enter manually

  getBookingDetails(invoiceNo: InvoiceNumberInterface) {
    this.bookingService.getBookingWithParams(invoiceNo).subscribe((res) => {
      this.bookingData = [];
      if (res.length > 0) {
        this.invoice = true;
        this.bookingData = res;
        this.updateServiceProvided(this.bookingData);
      }
      this.calculateInvoiceTotal(this.bookingData);

      this.initializeForm(this.bookingData, this.stockDetail);
    });
  }

  updateServiceProvided(bookingData:any){
    this.serviceProvided = [];
    if(bookingData){
      bookingData.forEach( (book:any,i:number = 0) => {
        this.serviceProvided[i] = {service_id: book.service_id}
        i++;
      });

    } else {

    }

  }

  // Get Invoice Details on Invoice Change
  getBookingOnInvoiceChange(event: any) {
    this.invoiceNo.invoice_number = event.target.value;
    this.getBookingDetails(this.invoiceNo);
  }

  // forTesting() {
  //   this.invoiceNo.invoice_number = '1';
  //   this.getBookingDetails(this.invoiceNo);
  // }

  //Getting all Customer
  getAllCustomer() {
    this.customerDetailStatus = true;
    this.customerService.getAllCustomer().subscribe((res) => {
      this.customerDetails = [];
      this.customerDetails = res;
      this.customerDetailStatus = false;
    });
  }

  //Getting all stock
  getStockDetails() {
    this.stockService.getAllStock().subscribe((res) => {
      this.stockDetails = [];
      if(res.length > 0){

        this.stockDetails = res;
      } else {
        this.sweetAlert.alert('No Stock is Created Yet','Create Stock From Stock Master Section','warning');
      }

    });
  }

  //Getting Booking Details for edit
  getParticularBookingDetails(bookingId: number) {
    this.bookingService.getBooking(bookingId).subscribe((res) => {
      this.editBookingStatus = true;
      this.initializeFormEditBooking(res);
    });
  }

  // ------------ Customer autocomplete ------------ //

  customerKeyword = 'customer_name';

  onChangeCustomer(search: string, type: string) {
    this.customerDetail = [];
    this.customerIsSelected = false;
    // const customer: CustomerInterface[] = this.customerDetails.filter(
    //   (x: { customer_name: string }) =>
    //     x.customer_name.toLowerCase() === search.toLowerCase()
    // );
    //
    // if (customer[0]) {
    //   this.customerIsSelected = true;
    //   this.customerDetail = customer;
    //   this.editCustomerDetail = customer;
    //   if (type == 'create') {
    //     this.setCustomerValues(this.createBooking, this.customerDetail[0]);
    //   } else {
    //     this.setCustomerValues(
    //       this.editBookingCustomer,
    //       this.customerDetail[0]
    //     );
    //   }
    // } else {
    //   let data: CustomerInterface = {
    //     customer_name: '',
    //     customer_address: '',
    //     customer_contact_no: 0,
    //     customer_gstin: 0,
    //   };
    //   if (type == 'create') {
    //     this.setCustomerValues(this.createBooking, data);
    //   } else {
    //     this.setCustomerValues(this.editBookingCustomer, data);
    //   }
    // }
  }

  setCustomerValues(type: any, data: any) {
    type.get('customer_address').setValue(data.customer_address);
    type.get('customer_contact_no').setValue(data.customer_contact_no);
    type.get('customer_gstin').setValue(data.customer_gstin);
  }

  selectEventCustomer(item: any, type: string) {
    this.customerDetail = item;
    this.customerIsSelected = true;
    if (type == 'create') {
      this.setCustomerValues(this.createBooking, item);
    } else {
      this.setCustomerValues(this.editBookingCustomer, item);
    }
    this.serviceFocus();
  }

  onCustomerCleared() {
    let data: CustomerInterface = {
      customer_name: '',
      customer_address: '',
      customer_contact_no: 0,
      customer_gstin: 0,
    };
    this.setCustomerValues(this.createBooking, data);
  }

  customerFilter = function (
    customerDetails: any[],
    query: string
  ): CustomerInterface[] {
    return  customerDetails.filter((x) => x.customer_name.toLowerCase().startsWith(query)
    );
  };

  // ------------ Service / Stock autocomplete ------------ //

  stockKeyword = 'stock_name';

  @ViewChild('service') service: any;

  serviceFocus() {
    this.service.focus();
  }

  setStockId(stockName:string){

    const stock = this.stockDetails.filter(
      (x: { stock_name: string }) =>
        x.stock_name.toLowerCase() === stockName.toLowerCase()
    );

    if(stock[0].id){
      this.service_id = stock[0].id;
    }


  }

  onChangeStock(search: string, type: string) {
    this.tabIndexStock = true;
    this.stockDetailStatus = true;
    this.stockAvailable = 0;

    const stock = this.stockDetails.filter(
      (x: { stock_name: string }) =>
        x.stock_name.toLowerCase() === search.toLowerCase()
    );

    if (this.service_id) {
      this.service_id = stock[0].id;
      this.isInventory = stock[0].is_inventory == 0;
      this.isInventoryValue = stock[0].is_inventory;
      this.stockDetailStatus = false;

      if (type == 'create') {
        this.createBooking.get('rate').setValue(stock[0].stock_mrp);
        this.createBooking.get('stock_available').setValue(stock[0].stock_available);
      } else {
        this.editBooking.get('rate').setValue(stock[0].stock_mrp);
        this.editBooking.get('stock_available').setValue(stock[0].stock_available);

      }
      if (this.isInventory) {
        if (type == 'create') {
          this.createBooking.get('qty_out').setValue(1);
          this.createBooking.get('stock_available').setValue(1);

        } else {
          this.editBooking.get('qty_out').setValue(1);
          this.editBooking.get('stock_available').setValue(1);
        }
      }

      this.stockAvailable = stock[0].stock_available;
    } else {
      this.createBooking.get('rate').setValue('');
      this.createBooking.get('qty_out').setValue('');
      this.createBooking.get('stock_available').setValue('');
    }

    this.currentBookingTotal();
  }

  selectEventStock(item: any, type: string) {

    this.service_id = item.id;

    let isServiceProvided
    this.serviceProvided.forEach( (x: any) =>{
      if(x.service_id == this.service_id){
        isServiceProvided = x.service_id
      }
    });

    if(!isServiceProvided){
      this.tabIndexStock = true;
      this.stockDetailStatus = false;
      this.isInventory = item.is_inventory == 0;
      this.isInventoryValue = item.is_inventory;
      this.tabIndexStockInventory = !this.isInventory;


      if (type == 'create') {
        this.createBooking.get('rate').setValue(item.stock_mrp);
        this.createBooking.get('stock_available').setValue(item.stock_available);
      } else {
        this.editBooking.get('rate').setValue(item.stock_mrp);
        this.editBooking.get('stock_available').setValue(item.stock_available);
      }

      if (this.isInventory) {
        if (type == 'create') {
          this.createBooking.get('qty_out').setValue(1);
          this.createBooking.get('stock_available').setValue(1);
        } else {
          this.editBooking.get('qty_out').setValue(1);
          this.editBooking.get('stock_available').setValue(1);
        }
      }

      this.stockAvailable = item.stock_available;
      this.currentBookingTotal();

    } else {
      this.sweetAlert.alert('Duplicate Service Selected','you can provide only single service per booking','warning')
    }
  }

  setStockValues(type: any, data: any) {
    type.get('qty_out').setValue(data.qty_out);
    type.get('rate').setValue(data.rate);
    type.get('book_stock_available').setValue(data.stock_available);
  }

  onStockCleared() {
    this.isInventory = false;
    this.stockDetailStatus = this.tabIndexStock = true;
    this.service_id = 0;
    this.createBooking.get('rate').setValue('');
    this.createBooking.get('qty_out').setValue('');
    this.createBooking.get('book_stock_available').setValue('');
    this.currentBookingTotal();
  }

  stockFilter = function ( stockDetails: any[], query: string ): StockInterface[] {
    return stockDetails.filter((x) =>
      x.stock_name.toLowerCase().startsWith(query)
    );
  };


  // ------------  Form Initialize ------------ //

  //Initialize create booking form
  initializeForm( bookingData: BookingInterface[], stockDetails: StockInterface[] ) {
    this.createBooking = this.formBuilder.group({
      invoice_number: [bookingData[0]['invoice_number'], [Validators.required]],
      invoice_date: [bookingData[0]['invoice_date'], [Validators.required]],
      customer_name: [bookingData[0]['customer_name'], [Validators.required]],
      customer_address: [ bookingData[0]['customer_address'], [Validators.required]],
      customer_contact_no: [bookingData[0]['customer_contact_no'],[Validators.required]],
      customer_gstin: [bookingData[0]['customer_gstin'],[Validators.nullValidator]],
      type: [1,[Validators.required]],
      service_id: [stockDetails[0]['id'], [Validators.required]],
      qty_out: ['', [Validators.required]],
      stock_available: [0, [Validators.required]],
      rate: [stockDetails[0]['stock_mrp'], [Validators.required]],
      extra_rate: [0, [Validators.required]],
      date: [this.datePipe.transform(this.today, 'yyyy-MM-dd'),[Validators.required]],
      days: ['', [Validators.required]],
      time: ['full', [Validators.required]],
      is_inventory: [],
    });
  }

  // Initialize Edit booking Form
  initializeFormEditBooking(bookingData: BookingInterface[] | any) {
    if(bookingData.stock_name){
      this.setStockId(bookingData.stock_name);
    }

    this.editBooking = this.formBuilder.group({
      service_id: [bookingData.stock_name, [Validators.required]],
      qty_out: [bookingData.qty_out, [Validators.required]],
      rate: [bookingData.rate, [Validators.required]],
      extra_rate: [bookingData.extra_rate, [Validators.required]],
      date: [bookingData.date, [Validators.required]],
      days: [bookingData.days, [Validators.required]],
      time: [bookingData.time, [Validators.required]],
      is_inventory: [bookingData.is_inventory],
    });
  }

  // Initialize Edit Customer Form
  customerEdit(customerDetails: CustomerInterface[] | any) {
    this.editBookingCustomer = this.formBuilder.group({
      customer_name: [customerDetails.customer_name, [Validators.required]],
      customer_address: [customerDetails.customer_address,[Validators.required]],
      customer_contact_no: [customerDetails.customer_contact_no,[Validators.required]],
      customer_gstin: [ customerDetails.customer_gstin,[Validators.nullValidator]],
    });
  }

  // ------------ Creating / Updating Booking Details ------------ //

  // Creating Customer Booking
  createCustomerBooking() {
    this.BookingCreatedSuccess = this.bookingDeleteSuccess = this.editCustomerSuccess = false;

    if (this.createBooking.valid) {

      if (this.invoiceNo.invoice_id) {
        this.createBooking.value.invoice_id = this.invoiceNo.invoice_id;
      }

      this.createBooking.value.service_id = this.service_id;
      this.createBooking.value.total = (this.createBooking.value.rate + this.createBooking.value.extra_rate) * this.createBooking.value.qty_out * this.createBooking.value.days;

      if (this.createBooking.value.customer_name.customer_name) {
        this.createBooking.value.customer_name = this.createBooking.value.customer_name.customer_name;
      }

      this.createBooking.value.is_inventory = this.isInventoryValue;
      this.createBooking.value.is_inventory = this.isInventoryValue;

      this.createBooking.value.remaining_stock = this.createBooking.value.stock_available - this.createBooking.value.qty_out;

      this.bookingService.createBooking(this.createBooking.value).subscribe((res) => {

        this.BookingCreatedSuccess = true;
        this.invoiceNo.invoice_number = res[0].invoice_number;
        this.invoiceNo.invoice_id = res[0].invoice_id;
        this.bookingData = [];
        this.bookingData = res;
        this.invoice = true;

        this.calculateInvoiceTotal(this.bookingData);
        this.getStockDetails();

        this.stockDetail = [
          {
            firm_id: 0,
            stock_name: '',
            stock_mrp: 0,
            stock_qty:0,
            stock_gst: 0,
            stock_type: false,
            is_inventory: 0,
            stock_value: 0,
          },
        ];

        this.initializeForm(this.bookingData,this.stockDetail);
        this.updateServiceProvided(this.bookingData);
        // console.log(this.serviceProvided);
        this.service.focus();
      });
    }
  }

  isServiceProvided(serviceId:number): string{
    return this.serviceProvided[serviceId].service_title;
  }

  // Updating Customer On Invoice
  customerEditOnInvoice() {
    this.BookingCreatedSuccess = this.bookingDeleteSuccess = this.editCustomerSuccess = false;

    if (this.editBookingCustomer.valid) {

      if (this.editBookingCustomer.value.customer_name.customer_name) {
        this.editBookingCustomer.value.customer_name = this.editBookingCustomer.value.customer_name.customer_name;
      }

      this.bookingService.updateCustomerOnInvoice( this.editBookingCustomer.value, this.invoiceNo.invoice_number).subscribe(() => {
          this.editCustomerSuccess = true;
          this.getBookingDetails(this.invoiceNo);
          this.getAllCustomer();
      });
    }
  }

  //Updating Booking Values after edit
  editCustomerBooking() {
    this.BookingCreatedSuccess = this.bookingDeleteSuccess = this.editCustomerSuccess = this.editBookingSuccess = false;

    if (this.editBooking.valid) {

      this.editBooking.value.service_id = this.service_id;

      this.editBooking.value.total = (this.editBooking.value.rate + this.editBooking.value.extra_rate)
        * this.editBooking.value.qty_out * this.editBooking.value.days;

      this.bookingService.editBooking(this.editBooking.value, this.bookingId).subscribe((res) => {
          this.invoice = true;
          this.editBookingStatus = false;
          this.editBookingSuccess = true;
          this.getBookingDetails(this.invoiceNo);
          this.getStockDetails();
        });
    }
  }

  // ------------ Managing Models ------------ //

  openEditBookingModel(editBookingModel: any, bookingId: number) {
    this.bookingId = bookingId;
    this.getParticularBookingDetails(this.bookingId);
    this.modalService.open(editBookingModel);
  }

  openCustomerEditModel(content: any) {
    this.modalService.open(content);
  }

  // ------------ Validation simplifier ------------ //

  formValidation(fieldName: string) {

    return (
      this.createBooking.controls[fieldName].errors &&
      this.createBooking.controls[fieldName].touched
    );
  }

  editBookingValidation(fieldName: string) {
    return (
      this.editBooking.controls[fieldName].errors &&
      this.editBooking.controls[fieldName].touched
    );
  }

  editBookingCustomerValidation(fieldName: string) {
    return (
      this.editBookingCustomer.controls[fieldName].errors &&
      this.editBookingCustomer.controls[fieldName].touched
    );
  }

  // ------------ Delete Booking ------------ //

  deleteBooking(bookingId: number,is_inventory:number) {
    this.bookingService.deleteBooking(bookingId,is_inventory).subscribe(() => {
      this.bookingDeleteSuccess = true;
      this.serviceProvided = [];
      this.getStockDetails();
      this.getBookingDetails(this.invoiceNo);

    });
  }

  // ------------ Reset Form ------------ //
  resetInvoice() {

    this.BookingCreatedSuccess = this.bookingDeleteSuccess = this.editCustomerSuccess = this.invoice = false;
    this.bookingData = [
      {
        id: 0,
        invoice_number: 1,
        invoice_id: 0,
        invoice_date: this.datePipe.transform(this.today, 'yyyy-MM-dd'),
        customer_name: '',
        customer_address: '',
        customer_contact_no: 0,
        customer_gstin: 0,
        stock_name: '',
        rate: 0,
        qty_out: 0,
        date: '',
        days: 0,
        time: '',
      },
    ];
    this.generateInvoiceNo();
  }

  // ------------ Booking Amount Total ------------ //

  currentBookingTotal(){
    let extra_rate = +this.createBooking.get('extra_rate').value;
    let rate = +this.createBooking.get('rate').value;

    rate = rate + extra_rate;
    // console.log(rate);
    let qty = +this.createBooking.get('qty_out').value;
    let days = +this.createBooking.get('days').value;
    this.currentTotal =  qty * rate * days;
  }
  calculateInvoiceTotal(bookingData: BookingInterface[] | any) {
    this.InvoiceTotalAmount = 0;
    for (let j = 0; j < this.bookingData.length; j++) {
      this.InvoiceTotalAmount += bookingData[j].total;
    }
  }
}
