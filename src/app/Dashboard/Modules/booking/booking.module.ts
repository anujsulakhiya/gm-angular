import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BookingRoutingModule } from './booking-routing.module';
import { BookingComponent } from './booking.component';
import { CreateBookingComponent } from './create-booking/create-booking.component';
import { EditBookingComponent } from './edit-booking/edit-booking.component';
import { BookingListComponent } from './booking-list/booking-list.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';
import {NgbButtonsModule} from "@ng-bootstrap/ng-bootstrap";

@NgModule({
  declarations: [
    BookingComponent,
    CreateBookingComponent,
    EditBookingComponent,
    BookingListComponent,
  ],
    imports: [
        CommonModule,
        BookingRoutingModule,
        ReactiveFormsModule,
        AutocompleteLibModule,
        NgbButtonsModule,
    ],
})
export class BookingModule {}
