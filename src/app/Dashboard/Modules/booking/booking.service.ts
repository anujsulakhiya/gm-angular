import { Injectable } from '@angular/core';
import { HttpService } from '../../../services/http.service';
import {
  BookingInterface,
  CreateBookingInterface,
  CreateCustomerInterface,
  CustomerInterface,
  LatestInvoiceInterface,
} from '../../dashboard.interfaces';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class BookingService {
  constructor(private httpService: HttpService) {}

  createBooking(data: CreateBookingInterface): Observable<BookingInterface[]> {
    return this.httpService.post('/booking', data);
  }

  editBooking(
    data: CreateBookingInterface,
    bookingId: number
  ): Observable<BookingInterface> {
    return this.httpService.put('/booking/' + bookingId, data);
  }

  updateCustomerOnInvoice(
    data: CreateCustomerInterface,
    invoice: number
  ): Observable<CustomerInterface> {
    return this.httpService.put(
      '/booking/updateCustomerOnInvoice/' + invoice,
      data
    );
  }

  getBookingWithParams(params: any): Observable<BookingInterface[]> {
    return this.httpService.getWithParams('/booking', params);
  }

  getBooking(bookingId: number): Observable<BookingInterface[]> {
    return this.httpService.get('/booking/' + bookingId);
  }

  getAllBooking(): Observable<BookingInterface[]> {
    return this.httpService.get('/booking/');
  }

  getLatestInvoice(): Observable<LatestInvoiceInterface> {
    return this.httpService.get('/getLatestInvoice');
  }

  deleteBooking(bookingId: number,is_inventory:number) {
    return this.httpService.delete('/booking/' + bookingId+'/'+is_inventory);
  }
}
