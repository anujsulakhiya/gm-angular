import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {CreateStockReturnComponent} from "./create-stock-return/create-stock-return.component";
import {StockReturnComponent} from "./stock-return.component";

const routes: Routes = [
  {
    path: '',
    component: StockReturnComponent,
    children: [
      {
        path: '',
        redirectTo: 'CreateStockReturn',
        pathMatch: 'full',
      },
      {
        path: 'CreateStockReturn',
        component: CreateStockReturnComponent,
      },

      {
        path: 'CreateStockReturn/:invoice_number',
        component: CreateStockReturnComponent,
      }
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StockReturnRoutingModule { }
