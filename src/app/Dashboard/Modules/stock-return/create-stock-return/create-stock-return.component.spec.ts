import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateStockReturnComponent } from './create-stock-return.component';

describe('CreateStockReturnComponent', () => {
  let component: CreateStockReturnComponent;
  let fixture: ComponentFixture<CreateStockReturnComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateStockReturnComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateStockReturnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
