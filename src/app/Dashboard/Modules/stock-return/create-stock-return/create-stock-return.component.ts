import { Component, OnInit } from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {
  BookingInterface, CustomerInterface,
  InvoiceInterface,
  InvoiceNumberInterface, StockReturnInterface,

} from "../../../dashboard.interfaces";
import {StockReturnService} from "../stock-return.service";
import {BookingService} from "../../booking/booking.service";
import {DatePipe} from "@angular/common";
import {GlobalService} from "../../../../services/global.service";
import {AlertService} from "../../../../services/alert.service";
import {NgbModal, NgbPopover} from "@ng-bootstrap/ng-bootstrap";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-create-stock-return',
  templateUrl: './create-stock-return.component.html',
  styleUrls: ['./create-stock-return.component.css']
})
export class CreateStockReturnComponent implements OnInit {

  //Today Date
  today: any = new Date();

  //Form Variable
  stockReturn: FormGroup|any;
  editStockReturn: FormGroup|any;

  //Stock return Variables
  stockReturnDetails: StockReturnInterface[] = [];
  bookingDetails: BookingInterface[]|any = [];
  bookingDetail: BookingInterface[]|any = [];
  bookingDetailsObject: BookingInterface[]|any = [];

  invoiceNo: InvoiceNumberInterface = {
    invoice_number : '',
    type : 1,
  };

  customerDetail: CustomerInterface= {
    customer_name: '',
    customer_address: '',
    customer_contact_no: 0,
    customer_gstin: 0,
  };

  bookId = 0;
  invoiceId = 0;
  serviceId = 0;
  currentTotal = 0;
  total_qty_remaining = 0;
  qty_remaining = 0;
  totalAmountPopover = '';

  //Status Variable
  stockReturnSuccess = false;
  invoice = false;
  totalStatus:any = false;
  submitted = false;
  enterLostValue = true;
  stockReturnAvailable = true;
  routeParams = false;

  constructor(private datePipe: DatePipe,
              private formBuilder: FormBuilder,
              private stockReturnService: StockReturnService,
              private bookingService: BookingService,
              private sweetAlert: AlertService,
              private modalService: NgbModal,
              private route: ActivatedRoute) {
    this.setDate();
    this.initializeForm();
    // this.getBookingDetails(this.invoiceNo);
    this.getRouteParam();
  }

  ngOnInit() {
    this.stockReturn = this.formBuilder.group({
      stock : new FormArray([])
    });
  }

  getRouteParam() {
    this.route.params.subscribe(params => {
      if(params.invoice_number){
        this.routeParams = true;
        this.invoiceNo.invoice_number = params.invoice_number;
        this.invoiceNo.type = 1;
        this.getBookingDetails(this.invoiceNo);
      } else {
        this.routeParams = false;
      }
    });
  }

  generateFields(bookingDetails: BookingInterface): FormGroup {
    return this.formBuilder.group({
      type: [2,Validators.required],
      is_inventory: [bookingDetails.is_inventory,Validators.required],
      invoice_number: [bookingDetails.invoice_number,Validators.required],
      invoice_id: [bookingDetails.invoice_id,Validators.required],
      service_name: [bookingDetails.stock_name, [Validators.nullValidator]],
      service_id: [bookingDetails.service_id, [Validators.nullValidator]],
      rate: [bookingDetails.rate, [Validators.nullValidator]],
      extra_rate: [bookingDetails.extra_rate, [Validators.nullValidator]],
      qty_out: [bookingDetails.qty_out, [Validators.nullValidator]],
      qty_remaining: [bookingDetails.qty_remaining, [Validators.nullValidator]],
      qty_in: [0, [Validators.required,Validators.min(0),Validators.max(<number>bookingDetails.qty_remaining)]],
      qty_lost: [0, [Validators.required,Validators.min(0)]],
      lost_value: [bookingDetails.stock_value, [Validators.nullValidator,Validators.min(0)]],
      book_date: [bookingDetails.date, [Validators.required]],
      date: [this.today, [Validators.required]],
      days: ['', [Validators.required]],
      total: [0, [Validators.required,Validators.min(0)]],
    })
  }

  editStockReturnInit(bookingDetails: any){
    this.editStockReturn = this.formBuilder.group({
      type: [2,Validators.required],
      is_inventory: [bookingDetails.is_inventory,Validators.required],
      invoice_number: [bookingDetails.invoice_number,Validators.required],
      invoice_id: [bookingDetails.invoice_id,Validators.required],
      service_name: [bookingDetails.stock_name, [Validators.nullValidator]],
      service_id: [bookingDetails.service_id, [Validators.nullValidator]],
      rate: [bookingDetails.rate, [Validators.nullValidator]],
      extra_rate: [bookingDetails.extra_rate, [Validators.nullValidator]],
      qty_out: [bookingDetails.qty_out, [Validators.nullValidator]],
      qty_remaining: [bookingDetails.qty_remaining, [Validators.nullValidator]],
      qty_in: [bookingDetails.qty_in, [Validators.required,Validators.min(0),Validators.max(<number>bookingDetails.qty_remaining)]],
      qty_lost: [bookingDetails.qty_lost, [Validators.required,Validators.min(0)]],
      lost_value: [bookingDetails.lost_value, [Validators.nullValidator,Validators.min(0)]],
      book_date: [bookingDetails.date, [Validators.required]],
      date: [bookingDetails.date, [Validators.required]],
      days: [bookingDetails.days, [Validators.required]],
      total: [bookingDetails.total, [Validators.required,Validators.min(0)]],
    })
  }

  get stock() : FormArray {
    return this.stockReturn.get("stock") as FormArray
  }

  onSubmit() {

  }


  // ------------ Set Current Date ------------ //
  setDate() {
    this.today = this.datePipe.transform(this.today, 'yyyy-MM-dd');
  }

  initializeForm() {

    this.stockReturn = this.formBuilder.group({
      stocks: new FormArray([]),
    });

  }

  getBookingDetails(invoiceNo: InvoiceNumberInterface) {

    this.invoice = false;

    this.invoiceNo.type = 1;

    this.stockReturnService.getStockReturnDetails(invoiceNo).subscribe((res) => {
      this.bookingDetails = [];
      if (res.length > 0) {

        this.invoice = true;
        this.bookingDetails = res;
        this.customerDetail = res[0];

        this.bookingDetails.forEach( (book: { id: string | number; }) => {
          this.bookingDetailsObject[book.id] = book;
        });

        this.stock.controls = [];
        this.bookingDetails.forEach( (book: BookingInterface) => {
          this.stock.push(this.generateFields(book));
        });

        this.qty_remaining = 0;
        this.stock.controls.forEach( (stock:any, i = 0) => {
              this.qty_remaining += stock.value.qty_remaining;
        });

        if(this.qty_remaining == 0){
          this.stockReturnAvailable = false;
        }

      }
      this.getStockReturnDetails(this.invoiceNo,2);
    });
  }

  getStockReturnDetails(invoiceNo: InvoiceNumberInterface,type:number){
    this.invoice = false;
    this.invoiceNo.invoice_number = invoiceNo.invoice_number;
    this.invoiceNo.type = type;



    this.stockReturnService.getStockReturnDetails(invoiceNo).subscribe((res) => {
      this.stockReturnDetails = [];

      if (res.length > 0) {
        this.invoice = true;
        this.stockReturnDetails = res;
      }

    });
  }

  // Get Invoice Details on Invoice Change
  getBookingOnInvoiceChange(event: any) {
    this.invoiceNo.invoice_number = event.target.value;
    this.invoiceNo.type = 1;
    this.getBookingDetails(this.invoiceNo);
  }

  checkStokeReturnStatus(index:any){
    return this.stock.controls[index].get('qty_remaining')?.value != 0;
  }

  editStockReturnModel(stockReturnStatusModel: any, bookId: any) {
    this.getBookingDetail(bookId);
    this.modalService.open(stockReturnStatusModel,{ size: 'xl', centered: true });
  }

  getBookingDetail(id: any){
    this.bookingService.getBooking(id).subscribe( res => {
      this.bookingDetail = res;
      this.editStockReturnInit(res);
    });
  }

  setValue(bookId: number, invoiceId: number, serviceId:number){
    this.bookId = bookId;
    this.invoiceId = invoiceId;
    this.serviceId = serviceId;
  }

  getBillDetails(bookId:any){
    return this.bookingDetailsObject[bookId];
  }

  updateStock(){

    const book = this.getBillDetails(this.bookId);

    this.stockReturn.value.invoice_id = book.invoice_id;
    this.stockReturn.value.service_id = book.service_id;
    this.stockReturn.value.rate = 0;
    this.stockReturn.value.days = book.days;

    this.stockReturnService.updateStock(this.stockReturn.value).subscribe( res => {
      this.stockReturnSuccess = true;
    });
  }

  updateSingleStock(i:any){
    this.totalStatus = false;
    this.totalStatus = this.stock.controls[i].get('total')?.invalid;

    if(this.stock.controls[i].valid){
      let qty = this.stock.controls[i].get('qty_in')?.value + this.stock.controls[i].get('qty_lost')?.value

      if(qty != 0){
        this.stockReturnService.updateStock(this.stock.controls[i].value).subscribe( res => {
          this.stockReturnSuccess = true;
          this.stockReturnDetails = res;

          this.getBookingDetails(this.invoiceNo);
        });
      } else {
        this.sweetAlert.alert('Enter valid Quantity','Quantity should be greater than zero','warning');
      }
    }
  }

  currentBookingTotal(index:any){

    let returnRate = this.stock.controls[index].get('rate')?.value + this.stock.controls[index].get('extra_rate')?.value;
    let returnDate = this.stock.controls[index].get('date')?.value;
    let qty = this.stock.controls[index].get('qty_in')?.value;

    let qty_lost = this.stock.controls[index].get('qty_lost')?.value;

    this.enterLostValue = qty_lost == 0;

    let lost_value = this.stock.controls[index].get('lost_value')?.value;
    let lost_amount = qty_lost * lost_value;
    let date = this.stock.controls[index].get('book_date')?.value;

    let days = this.calculateDiff(returnDate,date);

    this.stock.controls[index].get('days')?.setValue(days);
    this.currentTotal =  ( qty * returnRate * days ) + lost_amount;
    this.stock.controls[index].get('total')?.setValue(this.currentTotal);
    this.setRemainingQty(index);

  }
  editBookingTotal(){

    let returnRate = this.editStockReturn.get('rate')?.value + this.editStockReturn.get('extra_rate')?.value;
    let returnDate = this.editStockReturn.get('date')?.value;
    let qty = this.editStockReturn.get('qty_in')?.value;

    let qty_lost = this.editStockReturn.get('qty_lost')?.value;

    this.enterLostValue = qty_lost == 0;

    let lost_value = this.editStockReturn.get('lost_value')?.value;
    let lost_amount = qty_lost * lost_value;
    let date = this.editStockReturn.get('book_date')?.value;

    let days = this.calculateDiff(returnDate,date);

    this.editStockReturn.get('days')?.setValue(days);
    this.currentTotal =  ( qty * returnRate * days ) + lost_amount;
    this.editStockReturn.get('total')?.setValue(this.currentTotal);


    this.qty_remaining = 0;
    let qty_remaining = this.editStockReturn.get('qty_remaining')?.value;

    let qty_in = this.editStockReturn.get('qty_in')?.value;

    this.qty_remaining = qty_remaining - qty_in;

  }


  setRemainingQty(index:any){

    this.qty_remaining = 0;
    let qty_remaining = this.stock.controls[index].get('qty_remaining')?.value;

    let qty_in = this.stock.controls[index].get('qty_in')?.value;
    // let qty_lost = this.stock.controls[index].get('qty_lost')?.value;

    this.qty_remaining = qty_remaining - qty_in;
    // this.stock.controls[index].get('qty_remaining')?.setValue(this.qty_remaining);
  }

  setPreviousDate(date:any){
    let d = new Date(date);
    d.setDate(d.getDate() - 1);
    return d;
  }

  calculateDiff(currentDate:any,dateSent:any){

    currentDate = new Date(currentDate);
    dateSent = new Date(dateSent);

    return (Math.floor((Date.UTC(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate())
      - Date.UTC(dateSent.getFullYear(), dateSent.getMonth(), dateSent.getDate()) )
      /(1000 * 60 * 60 * 24 )))+1;
  }

  deleteStockReturn(bookId:any){
    this.stockReturnService.deleteStockReturn(bookId).subscribe( res => {
      this.getBookingDetails(this.invoiceNo);
    });
  }

  // ------------ Validation simplifier ------------ //

  formValidation(fieldName: any, index:any) {
    return (
      this.stock.controls[index].get(fieldName)?.invalid  &&  this.stock.controls[index].get(fieldName)?.touched
    );
  }
  editFormValidation(fieldName: any) {
    return (
      this.editStockReturn.get(fieldName)?.invalid  &&  this.editStockReturn.get(fieldName)?.touched
    );
  }
}
