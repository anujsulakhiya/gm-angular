import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StockReturnRoutingModule } from './stock-return-routing.module';
import { StockReturnComponent } from './stock-return.component';
import { CreateStockReturnComponent } from './create-stock-return/create-stock-return.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {NgbPopoverModule} from "@ng-bootstrap/ng-bootstrap";


@NgModule({
  declarations: [
    StockReturnComponent,
    CreateStockReturnComponent
  ],
  imports: [
    CommonModule,
    StockReturnRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    NgbPopoverModule
  ]
})
export class StockReturnModule { }
