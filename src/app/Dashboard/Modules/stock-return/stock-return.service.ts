import { Injectable } from '@angular/core';
import {HttpService} from "../../../services/http.service";
import {Observable} from "rxjs";
import {
  BookingInterface,
  InvoiceInterface,
  InvoiceNumberInterface,
  StockReturnInterface
} from "../../dashboard.interfaces";

@Injectable({
  providedIn: 'root'
})
export class StockReturnService {

  constructor(private httpService: HttpService) {}

  updateStock(data: StockReturnInterface): Observable<StockReturnInterface[]> {
    return this.httpService.post('/stock_return', data);
  }

  getStockReturnDetails(invoice_id: InvoiceNumberInterface): Observable<StockReturnInterface[]> {
    return this.httpService.getWithParams('/stock_return' ,invoice_id);
  }

  deleteStockReturn(stock_return_id: number) {
    return this.httpService.delete('/stock_return/' + stock_return_id);
  }
}
