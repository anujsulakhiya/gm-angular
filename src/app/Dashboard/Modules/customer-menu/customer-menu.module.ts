import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CustomerMenuRoutingModule } from './customer-menu-routing.module';
import { CustomerMenuComponent } from './customer-menu.component';
import { CreateCustomerMenuComponent } from './create-customer-menu/create-customer-menu.component';


@NgModule({
  declarations: [
    CustomerMenuComponent,
    CreateCustomerMenuComponent
  ],
  imports: [
    CommonModule,
    CustomerMenuRoutingModule
  ]
})
export class CustomerMenuModule { }
