import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateCustomerMenuComponent } from './create-customer-menu/create-customer-menu.component';
import { CustomerMenuComponent } from './customer-menu.component';

const routes: Routes = [
  {
    path: '',
    component: CustomerMenuComponent,
    children: [
      {
        path: '',
        redirectTo: 'CreateCustomerMenu',
        pathMatch: 'full',
      },
      {
        path: 'CreateCustomerMenu',
        component: CreateCustomerMenuComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomerMenuRoutingModule { }
