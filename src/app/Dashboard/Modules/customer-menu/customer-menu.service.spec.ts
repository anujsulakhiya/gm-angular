import { TestBed } from '@angular/core/testing';

import { CustomerMenuService } from './customer-menu.service';

describe('CustomerMenuService', () => {
  let service: CustomerMenuService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CustomerMenuService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
