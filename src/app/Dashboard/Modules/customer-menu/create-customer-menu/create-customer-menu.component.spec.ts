import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateCustomerMenuComponent } from './create-customer-menu.component';

describe('CreateCustomerMenuComponent', () => {
  let component: CreateCustomerMenuComponent;
  let fixture: ComponentFixture<CreateCustomerMenuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateCustomerMenuComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateCustomerMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
