import { NgModule } from '@angular/core';
import {CommonModule, DatePipe} from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { MenuComponent } from './inc/menu/menu.component';
import { HeaderComponent } from './inc/header/header.component';
import { FooterComponent } from './inc/footer/footer.component';
import { ChangePasswordComponent } from './Components/change-password/change-password.component';
import { ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HomeComponent } from './Components/home/home.component';
import { UserProfileComponent } from './Components/user-profile/user-profile.component';
import { MatIconModule } from '@angular/material/icon';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';
import {RouterModule} from "@angular/router";
import { CustomerLedgerComponent } from './Modules/customer-ledger/customer-ledger.component';
import { InvoiceFormateComponent } from './Modules/customer-ledger/invoice-formate/invoice-formate.component';

@NgModule({
  declarations: [
    DashboardComponent,
    HomeComponent,
    UserProfileComponent,
    MenuComponent,
    HeaderComponent,
    FooterComponent,
    ChangePasswordComponent,
    CustomerLedgerComponent,
    InvoiceFormateComponent,
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    ReactiveFormsModule,
    NgbModule,
    MatIconModule,
    AutocompleteLibModule,
      RouterModule
  ],
  providers:[DatePipe]
})
export class DashboardModule { }
